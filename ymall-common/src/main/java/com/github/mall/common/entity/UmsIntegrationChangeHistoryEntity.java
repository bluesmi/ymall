package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 积分变化历史记录表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_integration_change_history")
@ApiModel("积分变化历史记录表")
public class UmsIntegrationChangeHistoryEntity extends Model<UmsIntegrationChangeHistoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     * 改变类型：0->增加；1->减少
     */
    @TableField(value = "change_type")
    @ApiModelProperty(name = "changeType", value = "改变类型：0->增加；1->减少")
    private Integer changeType;
    /**
     * 积分改变数量
     */
    @TableField(value = "change_count")
    @ApiModelProperty(name = "changeCount", value = "积分改变数量")
    private Integer changeCount;
    /**
     * 操作人员
     */
    @TableField(value = "operate_man")
    @ApiModelProperty(name = "operateMan", value = "操作人员")
    private String operateMan;
    /**
     * 操作备注
     */
    @TableField(value = "operate_note")
    @ApiModelProperty(name = "operateNote", value = "操作备注")
    private String operateNote;
    /**
     * 积分来源：0->购物；1->管理员修改
     */
    @TableField(value = "source_type")
    @ApiModelProperty(name = "sourceType", value = "积分来源：0->购物；1->管理员修改")
    private Integer sourceType;

}
