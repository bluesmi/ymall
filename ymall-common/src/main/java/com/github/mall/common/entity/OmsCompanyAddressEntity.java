package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 公司收发货地址表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_company_address")
@ApiModel("公司收发货地址表")
public class OmsCompanyAddressEntity extends Model<OmsCompanyAddressEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 地址名称
     */
    @TableField(value = "address_name")
    @ApiModelProperty(name = "addressName", value = "地址名称")
    private String addressName;
    /**
     * 默认发货地址：0->否；1->是
     */
    @TableField(value = "send_status")
    @ApiModelProperty(name = "sendStatus", value = "默认发货地址：0->否；1->是")
    private Integer sendStatus;
    /**
     * 是否默认收货地址：0->否；1->是
     */
    @TableField(value = "receive_status")
    @ApiModelProperty(name = "receiveStatus", value = "是否默认收货地址：0->否；1->是")
    private Integer receiveStatus;
    /**
     * 收发货人姓名
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "收发货人姓名")
    private String name;
    /**
     * 收货人电话
     */
    @TableField(value = "phone")
    @ApiModelProperty(name = "phone", value = "收货人电话")
    private String phone;
    /**
     * 省/直辖市
     */
    @TableField(value = "province")
    @ApiModelProperty(name = "province", value = "省/直辖市")
    private String province;
    /**
     * 市
     */
    @TableField(value = "city")
    @ApiModelProperty(name = "city", value = "市")
    private String city;
    /**
     * 区
     */
    @TableField(value = "region")
    @ApiModelProperty(name = "region", value = "区")
    private String region;
    /**
     * 详细地址
     */
    @TableField(value = "detail_address")
    @ApiModelProperty(name = "detailAddress", value = "详细地址")
    private String detailAddress;

}
