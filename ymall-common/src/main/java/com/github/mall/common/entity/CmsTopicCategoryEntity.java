package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 话题分类表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_topic_category")
@ApiModel("话题分类表")
public class CmsTopicCategoryEntity extends Model<CmsTopicCategoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 分类图标
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "分类图标")
    private String icon;
    /**
     * 专题数量
     */
    @TableField(value = "subject_count")
    @ApiModelProperty(name = "subjectCount", value = "专题数量")
    private Integer subjectCount;
    /**
     *
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "")
    private Integer showStatus;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;

}
