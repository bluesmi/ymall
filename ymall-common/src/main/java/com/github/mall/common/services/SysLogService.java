package com.github.mall.common.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.mall.common.entity.SysLogEntity;

public interface SysLogService extends IService<SysLogEntity> {
}
