package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 相册表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_album")
@ApiModel("相册表")
public class PmsAlbumEntity extends Model<PmsAlbumEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     *
     */
    @TableField(value = "cover_pic")
    @ApiModelProperty(name = "coverPic", value = "")
    private String coverPic;
    /**
     *
     */
    @TableField(value = "pic_count")
    @ApiModelProperty(name = "picCount", value = "")
    private Integer picCount;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;
    /**
     *
     */
    @TableField(value = "description")
    @ApiModelProperty(name = "description", value = "")
    private String description;

}
