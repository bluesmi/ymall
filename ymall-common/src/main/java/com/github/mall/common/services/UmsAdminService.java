package com.github.mall.common.services;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.mall.common.entity.UmsAdminEntity;
import com.github.mall.common.entity.UmsPermissionEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

@Api(tags="用户service实现类")
public interface UmsAdminService extends IService<UmsAdminEntity> {

    @ApiOperation("根据用户名查找用户")
    @ApiParam(name = "username",value = "用户名")
    UmsAdminEntity getAdminByUsername(String username) ;

    @ApiOperation("根据用户标识查询用户权限")
    @ApiParam(name = "userId",value = "用户名")
    List<UmsPermissionEntity> getPermissionList(String userId);

    @ApiOperation("注册用户")
    UmsAdminEntity register(Dict umsAdminParam) ;

    @ApiOperation("登录")
    String login(Dict param);
}
