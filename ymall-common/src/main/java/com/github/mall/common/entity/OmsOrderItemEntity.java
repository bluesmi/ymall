package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 订单中所包含的商品
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order_item")
@ApiModel("订单中所包含的商品")
public class OmsOrderItemEntity extends Model<OmsOrderItemEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 订单id
     */
    @TableField(value = "order_id")
    @ApiModelProperty(name = "orderId", value = "订单id")
    private String orderId;
    /**
     * 订单编号
     */
    @TableField(value = "order_sn")
    @ApiModelProperty(name = "orderSn", value = "订单编号")
    private String orderSn;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "product_pic")
    @ApiModelProperty(name = "productPic", value = "")
    private String productPic;
    /**
     *
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "")
    private String productName;
    /**
     *
     */
    @TableField(value = "product_brand")
    @ApiModelProperty(name = "productBrand", value = "")
    private String productBrand;
    /**
     *
     */
    @TableField(value = "product_sn")
    @ApiModelProperty(name = "productSn", value = "")
    private String productSn;
    /**
     * 销售价格
     */
    @TableField(value = "product_price")
    @ApiModelProperty(name = "productPrice", value = "销售价格")
    private BigDecimal productPrice;
    /**
     * 购买数量
     */
    @TableField(value = "product_quantity")
    @ApiModelProperty(name = "productQuantity", value = "购买数量")
    private Integer productQuantity;
    /**
     * 商品sku编号
     */
    @TableField(value = "product_sku_id")
    @ApiModelProperty(name = "productSkuId", value = "商品sku编号")
    private String productSkuId;
    /**
     * 商品sku条码
     */
    @TableField(value = "product_sku_code")
    @ApiModelProperty(name = "productSkuCode", value = "商品sku条码")
    private String productSkuCode;
    /**
     * 商品分类id
     */
    @TableField(value = "product_category_id")
    @ApiModelProperty(name = "productCategoryId", value = "商品分类id")
    private String productCategoryId;
    /**
     * 商品的销售属性
     */
    @TableField(value = "sp1")
    @ApiModelProperty(name = "sp1", value = "商品的销售属性")
    private String sp1;
    /**
     *
     */
    @TableField(value = "sp2")
    @ApiModelProperty(name = "sp2", value = "")
    private String sp2;
    /**
     *
     */
    @TableField(value = "sp3")
    @ApiModelProperty(name = "sp3", value = "")
    private String sp3;
    /**
     * 商品促销名称
     */
    @TableField(value = "promotion_name")
    @ApiModelProperty(name = "promotionName", value = "商品促销名称")
    private String promotionName;
    /**
     * 商品促销分解金额
     */
    @TableField(value = "promotion_amount")
    @ApiModelProperty(name = "promotionAmount", value = "商品促销分解金额")
    private BigDecimal promotionAmount;
    /**
     * 优惠券优惠分解金额
     */
    @TableField(value = "coupon_amount")
    @ApiModelProperty(name = "couponAmount", value = "优惠券优惠分解金额")
    private BigDecimal couponAmount;
    /**
     * 积分优惠分解金额
     */
    @TableField(value = "integration_amount")
    @ApiModelProperty(name = "integrationAmount", value = "积分优惠分解金额")
    private BigDecimal integrationAmount;
    /**
     * 该商品经过优惠后的分解金额
     */
    @TableField(value = "real_amount")
    @ApiModelProperty(name = "realAmount", value = "该商品经过优惠后的分解金额")
    private BigDecimal realAmount;
    /**
     *
     */
    @TableField(value = "gift_integration")
    @ApiModelProperty(name = "giftIntegration", value = "")
    private Integer giftIntegration;
    /**
     *
     */
    @TableField(value = "gift_growth")
    @ApiModelProperty(name = "giftGrowth", value = "")
    private Integer giftGrowth;
    /**
     * 商品销售属性:[{"key":"颜色","value":"颜色"},{"key":"容量","value":"4G"}]
     */
    @TableField(value = "product_attr")
    @ApiModelProperty(name = "productAttr", value = "商品销售属性:颜色,容量")
    private String productAttr;

}
