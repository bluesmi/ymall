package com.github.mall.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.mall.common.entity.SysLogEntity;

public interface SysLogDao extends BaseMapper<SysLogEntity> {
}
