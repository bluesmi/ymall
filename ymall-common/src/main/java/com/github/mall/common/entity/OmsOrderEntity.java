package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order")
@ApiModel("订单表")
public class OmsOrderEntity extends Model<OmsOrderEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "订单id")
    private String id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     *
     */
    @TableField(value = "coupon_id")
    @ApiModelProperty(name = "couponId", value = "")
    private String couponId;
    /**
     * 订单编号
     */
    @TableField(value = "order_sn")
    @ApiModelProperty(name = "orderSn", value = "订单编号")
    private String orderSn;
    /**
     * 提交时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "提交时间")
    private Date createTime;
    /**
     * 用户帐号
     */
    @TableField(value = "member_username")
    @ApiModelProperty(name = "memberUsername", value = "用户帐号")
    private String memberUsername;
    /**
     * 订单总金额
     */
    @TableField(value = "total_amount")
    @ApiModelProperty(name = "totalAmount", value = "订单总金额")
    private BigDecimal totalAmount;
    /**
     * 应付金额（实际支付金额）
     */
    @TableField(value = "pay_amount")
    @ApiModelProperty(name = "payAmount", value = "应付金额（实际支付金额）")
    private BigDecimal payAmount;
    /**
     * 运费金额
     */
    @TableField(value = "freight_amount")
    @ApiModelProperty(name = "freightAmount", value = "运费金额")
    private BigDecimal freightAmount;
    /**
     * 促销优化金额（促销价、满减、阶梯价）
     */
    @TableField(value = "promotion_amount")
    @ApiModelProperty(name = "promotionAmount", value = "促销优化金额（促销价、满减、阶梯价）")
    private BigDecimal promotionAmount;
    /**
     * 积分抵扣金额
     */
    @TableField(value = "integration_amount")
    @ApiModelProperty(name = "integrationAmount", value = "积分抵扣金额")
    private BigDecimal integrationAmount;
    /**
     * 优惠券抵扣金额
     */
    @TableField(value = "coupon_amount")
    @ApiModelProperty(name = "couponAmount", value = "优惠券抵扣金额")
    private BigDecimal couponAmount;
    /**
     * 管理员后台调整订单使用的折扣金额
     */
    @TableField(value = "discount_amount")
    @ApiModelProperty(name = "discountAmount", value = "管理员后台调整订单使用的折扣金额")
    private BigDecimal discountAmount;
    /**
     * 支付方式：0->未支付；1->支付宝；2->微信
     */
    @TableField(value = "pay_type")
    @ApiModelProperty(name = "payType", value = "支付方式：0->未支付；1->支付宝；2->微信")
    private Integer payType;
    /**
     * 订单来源：0->PC订单；1->app订单
     */
    @TableField(value = "source_type")
    @ApiModelProperty(name = "sourceType", value = "订单来源：0->PC订单；1->app订单")
    private Integer sourceType;
    /**
     * 订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单")
    private Integer status;
    /**
     * 订单类型：0->正常订单；1->秒杀订单
     */
    @TableField(value = "order_type")
    @ApiModelProperty(name = "orderType", value = "订单类型：0->正常订单；1->秒杀订单")
    private Integer orderType;
    /**
     * 物流公司(配送方式)
     */
    @TableField(value = "delivery_company")
    @ApiModelProperty(name = "deliveryCompany", value = "物流公司(配送方式)")
    private String deliveryCompany;
    /**
     * 物流单号
     */
    @TableField(value = "delivery_sn")
    @ApiModelProperty(name = "deliverySn", value = "物流单号")
    private String deliverySn;
    /**
     * 自动确认时间（天）
     */
    @TableField(value = "auto_confirm_day")
    @ApiModelProperty(name = "autoConfirmDay", value = "自动确认时间（天）")
    private Integer autoConfirmDay;
    /**
     * 可以获得的积分
     */
    @TableField(value = "integration")
    @ApiModelProperty(name = "integration", value = "可以获得的积分")
    private Integer integration;
    /**
     * 可以活动的成长值
     */
    @TableField(value = "growth")
    @ApiModelProperty(name = "growth", value = "可以活动的成长值")
    private Integer growth;
    /**
     * 活动信息
     */
    @TableField(value = "promotion_info")
    @ApiModelProperty(name = "promotionInfo", value = "活动信息")
    private String promotionInfo;
    /**
     * 发票类型：0->不开发票；1->电子发票；2->纸质发票
     */
    @TableField(value = "bill_type")
    @ApiModelProperty(name = "billType", value = "发票类型：0->不开发票；1->电子发票；2->纸质发票")
    private Integer billType;
    /**
     * 发票抬头
     */
    @TableField(value = "bill_header")
    @ApiModelProperty(name = "billHeader", value = "发票抬头")
    private String billHeader;
    /**
     * 发票内容
     */
    @TableField(value = "bill_content")
    @ApiModelProperty(name = "billContent", value = "发票内容")
    private String billContent;
    /**
     * 收票人电话
     */
    @TableField(value = "bill_receiver_phone")
    @ApiModelProperty(name = "billReceiverPhone", value = "收票人电话")
    private String billReceiverPhone;
    /**
     * 收票人邮箱
     */
    @TableField(value = "bill_receiver_email")
    @ApiModelProperty(name = "billReceiverEmail", value = "收票人邮箱")
    private String billReceiverEmail;
    /**
     * 收货人姓名
     */
    @TableField(value = "receiver_name")
    @ApiModelProperty(name = "receiverName", value = "收货人姓名")
    private String receiverName;
    /**
     * 收货人电话
     */
    @TableField(value = "receiver_phone")
    @ApiModelProperty(name = "receiverPhone", value = "收货人电话")
    private String receiverPhone;
    /**
     * 收货人邮编
     */
    @TableField(value = "receiver_post_code")
    @ApiModelProperty(name = "receiverPostCode", value = "收货人邮编")
    private String receiverPostCode;
    /**
     * 省份/直辖市
     */
    @TableField(value = "receiver_province")
    @ApiModelProperty(name = "receiverProvince", value = "省份/直辖市")
    private String receiverProvince;
    /**
     * 城市
     */
    @TableField(value = "receiver_city")
    @ApiModelProperty(name = "receiverCity", value = "城市")
    private String receiverCity;
    /**
     * 区
     */
    @TableField(value = "receiver_region")
    @ApiModelProperty(name = "receiverRegion", value = "区")
    private String receiverRegion;
    /**
     * 详细地址
     */
    @TableField(value = "receiver_detail_address")
    @ApiModelProperty(name = "receiverDetailAddress", value = "详细地址")
    private String receiverDetailAddress;
    /**
     * 订单备注
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "订单备注")
    private String note;
    /**
     * 确认收货状态：0->未确认；1->已确认
     */
    @TableField(value = "confirm_status")
    @ApiModelProperty(name = "confirmStatus", value = "确认收货状态：0->未确认；1->已确认")
    private Integer confirmStatus;
    /**
     * 删除状态：0->未删除；1->已删除
     */
    @TableField(value = "delete_status")
    @ApiModelProperty(name = "deleteStatus", value = "删除状态：0->未删除；1->已删除")
    private Integer deleteStatus;
    /**
     * 下单时使用的积分
     */
    @TableField(value = "use_integration")
    @ApiModelProperty(name = "useIntegration", value = "下单时使用的积分")
    private Integer useIntegration;
    /**
     * 支付时间
     */
    @TableField(value = "payment_time")
    @ApiModelProperty(name = "paymentTime", value = "支付时间")
    private Date paymentTime;
    /**
     * 发货时间
     */
    @TableField(value = "delivery_time")
    @ApiModelProperty(name = "deliveryTime", value = "发货时间")
    private Date deliveryTime;
    /**
     * 确认收货时间
     */
    @TableField(value = "receive_time")
    @ApiModelProperty(name = "receiveTime", value = "确认收货时间")
    private Date receiveTime;
    /**
     * 评价时间
     */
    @TableField(value = "comment_time")
    @ApiModelProperty(name = "commentTime", value = "评价时间")
    private Date commentTime;
    /**
     * 修改时间
     */
    @TableField(value = "modify_time")
    @ApiModelProperty(name = "modifyTime", value = "修改时间")
    private Date modifyTime;

}
