package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 商品会员价格表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_member_price")
@ApiModel("商品会员价格表")
public class PmsMemberPriceEntity extends Model<PmsMemberPriceEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "member_level_id")
    @ApiModelProperty(name = "memberLevelId", value = "")
    private String memberLevelId;
    /**
     * 会员价格
     */
    @TableField(value = "member_price")
    @ApiModelProperty(name = "memberPrice", value = "会员价格")
    private BigDecimal memberPrice;
    /**
     *
     */
    @TableField(value = "member_level_name")
    @ApiModelProperty(name = "memberLevelName", value = "")
    private String memberLevelName;

}
