package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_operate_log")
@ApiModel("")
public class PmsProductOperateLogEntity extends Model<PmsProductOperateLogEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "price_old")
    @ApiModelProperty(name = "priceOld", value = "")
    private BigDecimal priceOld;
    /**
     *
     */
    @TableField(value = "price_new")
    @ApiModelProperty(name = "priceNew", value = "")
    private BigDecimal priceNew;
    /**
     *
     */
    @TableField(value = "sale_price_old")
    @ApiModelProperty(name = "salePriceOld", value = "")
    private BigDecimal salePriceOld;
    /**
     *
     */
    @TableField(value = "sale_price_new")
    @ApiModelProperty(name = "salePriceNew", value = "")
    private BigDecimal salePriceNew;
    /**
     * 赠送的积分
     */
    @TableField(value = "gift_point_old")
    @ApiModelProperty(name = "giftPointOld", value = "赠送的积分")
    private Integer giftPointOld;
    /**
     *
     */
    @TableField(value = "gift_point_new")
    @ApiModelProperty(name = "giftPointNew", value = "")
    private Integer giftPointNew;
    /**
     *
     */
    @TableField(value = "use_point_limit_old")
    @ApiModelProperty(name = "usePointLimitOld", value = "")
    private Integer usePointLimitOld;
    /**
     *
     */
    @TableField(value = "use_point_limit_new")
    @ApiModelProperty(name = "usePointLimitNew", value = "")
    private Integer usePointLimitNew;
    /**
     * 操作人
     */
    @TableField(value = "operate_man")
    @ApiModelProperty(name = "operateMan", value = "操作人")
    private String operateMan;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;

}
