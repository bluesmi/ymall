package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 专题评论表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_topic_comment")
@ApiModel("专题评论表")
public class CmsTopicCommentEntity extends Model<CmsTopicCommentEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_nick_name")
    @ApiModelProperty(name = "memberNickName", value = "")
    private String memberNickName;
    /**
     *
     */
    @TableField(value = "topic_id")
    @ApiModelProperty(name = "topicId", value = "")
    private String topicId;
    /**
     *
     */
    @TableField(value = "member_icon")
    @ApiModelProperty(name = "memberIcon", value = "")
    private String memberIcon;
    /**
     *
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "")
    private String content;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "")
    private Integer showStatus;

}
