package com.github.mall.common.security.bo;

import com.github.mall.common.constant.CommonConstants;
import com.github.mall.common.entity.UmsAdminEntity;
import com.github.mall.common.entity.UmsPermissionEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;



public class AdminUserDetails implements UserDetails {

    private final UmsAdminEntity umsAdmin;
    private final List<UmsPermissionEntity> permissionList;


    public AdminUserDetails(UmsAdminEntity umsAdmin, List<UmsPermissionEntity> permissionList) {
        this.umsAdmin = umsAdmin;
        this.permissionList = permissionList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的权限
        return permissionList.stream()
                .filter(permission -> permission.getValue()!=null)
                .map(permission ->new SimpleGrantedAuthority(permission.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return umsAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return umsAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return umsAdmin.getStatus() == CommonConstants.UMS_ADMIN_STATUS;
    }
}
