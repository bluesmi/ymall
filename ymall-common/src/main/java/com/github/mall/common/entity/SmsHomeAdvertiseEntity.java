package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 首页轮播广告表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_home_advertise")
@ApiModel("首页轮播广告表")
public class SmsHomeAdvertiseEntity extends Model<SmsHomeAdvertiseEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 轮播位置：0->PC首页轮播；1->app首页轮播
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "轮播位置：0->PC首页轮播；1->app首页轮播")
    private Integer type;
    /**
     *
     */
    @TableField(value = "pic")
    @ApiModelProperty(name = "pic", value = "")
    private String pic;
    /**
     *
     */
    @TableField(value = "start_time")
    @ApiModelProperty(name = "startTime", value = "")
    private Date startTime;
    /**
     *
     */
    @TableField(value = "end_time")
    @ApiModelProperty(name = "endTime", value = "")
    private Date endTime;
    /**
     * 上下线状态：0->下线；1->上线
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "上下线状态：0->下线；1->上线")
    private Integer status;
    /**
     * 点击数
     */
    @TableField(value = "click_count")
    @ApiModelProperty(name = "clickCount", value = "点击数")
    private Integer clickCount;
    /**
     * 下单数
     */
    @TableField(value = "order_count")
    @ApiModelProperty(name = "orderCount", value = "下单数")
    private Integer orderCount;
    /**
     * 链接地址
     */
    @TableField(value = "url")
    @ApiModelProperty(name = "url", value = "链接地址")
    private String url;
    /**
     * 备注
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "备注")
    private String note;
    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "排序")
    private Integer sort;

}
