package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 会员积分成长规则表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_rule_setting")
@ApiModel("会员积分成长规则表")
public class UmsMemberRuleSettingEntity extends Model<UmsMemberRuleSettingEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 连续签到天数
     */
    @TableField(value = "continue_sign_day")
    @ApiModelProperty(name = "continueSignDay", value = "连续签到天数")
    private Integer continueSignDay;
    /**
     * 连续签到赠送数量
     */
    @TableField(value = "continue_sign_point")
    @ApiModelProperty(name = "continueSignPoint", value = "连续签到赠送数量")
    private Integer continueSignPoint;
    /**
     * 每消费多少元获取1个点
     */
    @TableField(value = "consume_per_point")
    @ApiModelProperty(name = "consumePerPoint", value = "每消费多少元获取1个点")
    private BigDecimal consumePerPoint;
    /**
     * 最低获取点数的订单金额
     */
    @TableField(value = "low_order_amount")
    @ApiModelProperty(name = "lowOrderAmount", value = "最低获取点数的订单金额")
    private BigDecimal lowOrderAmount;
    /**
     * 每笔订单最高获取点数
     */
    @TableField(value = "max_point_per_order")
    @ApiModelProperty(name = "maxPointPerOrder", value = "每笔订单最高获取点数")
    private Integer maxPointPerOrder;
    /**
     * 类型：0->积分规则；1->成长值规则
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "类型：0->积分规则；1->成长值规则")
    private Integer type;

}
