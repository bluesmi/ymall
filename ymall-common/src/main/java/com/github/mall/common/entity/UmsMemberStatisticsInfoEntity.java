package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 会员统计信息
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_statistics_info")
@ApiModel("会员统计信息")
public class UmsMemberStatisticsInfoEntity extends Model<UmsMemberStatisticsInfoEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     * 累计消费金额
     */
    @TableField(value = "consume_amount")
    @ApiModelProperty(name = "consumeAmount", value = "累计消费金额")
    private BigDecimal consumeAmount;
    /**
     * 订单数量
     */
    @TableField(value = "order_count")
    @ApiModelProperty(name = "orderCount", value = "订单数量")
    private Integer orderCount;
    /**
     * 优惠券数量
     */
    @TableField(value = "coupon_count")
    @ApiModelProperty(name = "couponCount", value = "优惠券数量")
    private Integer couponCount;
    /**
     * 评价数
     */
    @TableField(value = "comment_count")
    @ApiModelProperty(name = "commentCount", value = "评价数")
    private Integer commentCount;
    /**
     * 退货数量
     */
    @TableField(value = "return_order_count")
    @ApiModelProperty(name = "returnOrderCount", value = "退货数量")
    private Integer returnOrderCount;
    /**
     * 登录次数
     */
    @TableField(value = "login_count")
    @ApiModelProperty(name = "loginCount", value = "登录次数")
    private Integer loginCount;
    /**
     * 关注数量
     */
    @TableField(value = "attend_count")
    @ApiModelProperty(name = "attendCount", value = "关注数量")
    private Integer attendCount;
    /**
     * 粉丝数量
     */
    @TableField(value = "fans_count")
    @ApiModelProperty(name = "fansCount", value = "粉丝数量")
    private Integer fansCount;
    /**
     *
     */
    @TableField(value = "collect_product_count")
    @ApiModelProperty(name = "collectProductCount", value = "")
    private Integer collectProductCount;
    /**
     *
     */
    @TableField(value = "collect_subject_count")
    @ApiModelProperty(name = "collectSubjectCount", value = "")
    private Integer collectSubjectCount;
    /**
     *
     */
    @TableField(value = "collect_topic_count")
    @ApiModelProperty(name = "collectTopicCount", value = "")
    private Integer collectTopicCount;
    /**
     *
     */
    @TableField(value = "collect_comment_count")
    @ApiModelProperty(name = "collectCommentCount", value = "")
    private Integer collectCommentCount;
    /**
     *
     */
    @TableField(value = "invite_friend_count")
    @ApiModelProperty(name = "inviteFriendCount", value = "")
    private Integer inviteFriendCount;
    /**
     * 最后一次下订单时间
     */
    @TableField(value = "recent_order_time")
    @ApiModelProperty(name = "recentOrderTime", value = "最后一次下订单时间")
    private Date recentOrderTime;

}
