package com.github.mall.common.log.aspect;

import com.github.mall.common.constant.enmus.KafkaTopicEnums;
import com.github.mall.common.entity.SysLogEntity;
import com.github.mall.common.kafka.util.KafkaConsumerUtil;
import com.github.mall.common.log.annotation.SysLog;
import com.github.mall.common.log.utils.SysLogUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author micy
 * 操作日志使用spring event异步入库
 */
@Aspect
@Slf4j
@AllArgsConstructor
public class SysLogAspect {

//    private final SysLogService sysLogService;


    @Around("@annotation(sysLog)")
    @SneakyThrows
    public Object around(ProceedingJoinPoint point, SysLog sysLog) {
        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);

        SysLogEntity logVo = SysLogUtils.getSysLog();
        logVo.setTitle(sysLog.value());
        // 发送异步日志事件
        Long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        Long endTime = System.currentTimeMillis();
        logVo.setTime(String.valueOf(endTime - startTime));
//        SpringContextHolder.publishEvent(new SysLogEvent(logVo));
        // 先定义直接插，后通过消息来实现
//        sysLogService.save(logVo);
        KafkaConsumerUtil.sendMessage(KafkaTopicEnums.SYS_LOG_TOPIC.getTopic(),logVo);
        return obj;
    }
}
