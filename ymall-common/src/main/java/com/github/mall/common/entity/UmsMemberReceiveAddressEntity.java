package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 会员收货地址表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_receive_address")
@ApiModel("会员收货地址表")
public class UmsMemberReceiveAddressEntity extends Model<UmsMemberReceiveAddressEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     * 收货人名称
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "收货人名称")
    private String name;
    /**
     *
     */
    @TableField(value = "phone_number")
    @ApiModelProperty(name = "phoneNumber", value = "")
    private String phoneNumber;
    /**
     * 是否为默认
     */
    @TableField(value = "default_status")
    @ApiModelProperty(name = "defaultStatus", value = "是否为默认")
    private Integer defaultStatus;
    /**
     * 邮政编码
     */
    @TableField(value = "post_code")
    @ApiModelProperty(name = "postCode", value = "邮政编码")
    private String postCode;
    /**
     * 省份/直辖市
     */
    @TableField(value = "province")
    @ApiModelProperty(name = "province", value = "省份/直辖市")
    private String province;
    /**
     * 城市
     */
    @TableField(value = "city")
    @ApiModelProperty(name = "city", value = "城市")
    private String city;
    /**
     * 区
     */
    @TableField(value = "region")
    @ApiModelProperty(name = "region", value = "区")
    private String region;
    /**
     * 详细地址(街道)
     */
    @TableField(value = "detail_address")
    @ApiModelProperty(name = "detailAddress", value = "详细地址(街道)")
    private String detailAddress;

}
