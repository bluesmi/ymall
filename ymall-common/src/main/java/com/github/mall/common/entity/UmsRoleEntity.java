package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 后台用户角色表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_role")
@ApiModel("后台用户角色表")
public class UmsRoleEntity extends Model<UmsRoleEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 名称
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "名称")
    private String name;
    /**
     * 描述
     */
    @TableField(value = "description")
    @ApiModelProperty(name = "description", value = "描述")
    private String description;
    /**
     * 后台用户数量
     */
    @TableField(value = "admin_count")
    @ApiModelProperty(name = "adminCount", value = "后台用户数量")
    private Integer adminCount;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Date createTime;
    /**
     * 启用状态：0->禁用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "启用状态：0->禁用；1->启用")
    private Integer status;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;

}
