package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 退货原因表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order_return_reason")
@ApiModel("退货原因表")
public class OmsOrderReturnReasonEntity extends Model<OmsOrderReturnReasonEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 退货类型
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "退货类型")
    private String name;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;
    /**
     * 状态：0->不启用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "状态：0->不启用；1->启用")
    private Integer status;
    /**
     * 添加时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "添加时间")
    private Date createTime;

}
