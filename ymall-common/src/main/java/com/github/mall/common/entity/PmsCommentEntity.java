package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 商品评价表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_comment")
@ApiModel("商品评价表")
public class PmsCommentEntity extends Model<PmsCommentEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "member_nick_name")
    @ApiModelProperty(name = "memberNickName", value = "")
    private String memberNickName;
    /**
     *
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "")
    private String productName;
    /**
     * 评价星数：0->5
     */
    @TableField(value = "star")
    @ApiModelProperty(name = "star", value = "评价星数：0->5")
    private Integer star;
    /**
     * 评价的ip
     */
    @TableField(value = "member_ip")
    @ApiModelProperty(name = "memberIp", value = "评价的ip")
    private String memberIp;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "")
    private Integer showStatus;
    /**
     * 购买时的商品属性
     */
    @TableField(value = "product_attribute")
    @ApiModelProperty(name = "productAttribute", value = "购买时的商品属性")
    private String productAttribute;
    /**
     *
     */
    @TableField(value = "collect_couont")
    @ApiModelProperty(name = "collectCouont", value = "")
    private Integer collectCouont;
    /**
     *
     */
    @TableField(value = "read_count")
    @ApiModelProperty(name = "readCount", value = "")
    private Integer readCount;
    /**
     *
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "")
    private String content;
    /**
     * 上传图片地址，以逗号隔开
     */
    @TableField(value = "pics")
    @ApiModelProperty(name = "pics", value = "上传图片地址，以逗号隔开")
    private String pics;
    /**
     * 评论用户头像
     */
    @TableField(value = "member_icon")
    @ApiModelProperty(name = "memberIcon", value = "评论用户头像")
    private String memberIcon;
    /**
     *
     */
    @TableField(value = "replay_count")
    @ApiModelProperty(name = "replayCount", value = "")
    private Integer replayCount;

}
