package com.github.mall.common.dao;

import com.github.mall.common.entity.CmsSubjectProductRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品关系表
 * 
 * @author bluesmi
 * @email mi961212y@gmail.com
 * @date 2019-04-22 11:29:36
 */
@Mapper
public interface CmsSubjectProductRelationDao extends BaseMapper<CmsSubjectProductRelationEntity> {
	
}
