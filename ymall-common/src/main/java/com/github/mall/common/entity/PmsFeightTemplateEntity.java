package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 运费模版
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_feight_template")
@ApiModel("运费模版")
public class PmsFeightTemplateEntity extends Model<PmsFeightTemplateEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 计费类型:0->按重量；1->按件数
     */
    @TableField(value = "charge_type")
    @ApiModelProperty(name = "chargeType", value = "计费类型:0->按重量；1->按件数")
    private Integer chargeType;
    /**
     * 首重kg
     */
    @TableField(value = "first_weight")
    @ApiModelProperty(name = "firstWeight", value = "首重kg")
    private BigDecimal firstWeight;
    /**
     * 首费（元）
     */
    @TableField(value = "first_fee")
    @ApiModelProperty(name = "firstFee", value = "首费（元）")
    private BigDecimal firstFee;
    /**
     *
     */
    @TableField(value = "continue_weight")
    @ApiModelProperty(name = "continueWeight", value = "")
    private BigDecimal continueWeight;
    /**
     *
     */
    @TableField(value = "continme_fee")
    @ApiModelProperty(name = "continmeFee", value = "")
    private BigDecimal continmeFee;
    /**
     * 目的地（省、市）
     */
    @TableField(value = "dest")
    @ApiModelProperty(name = "dest", value = "目的地（省、市）")
    private String dest;

}
