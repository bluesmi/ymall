package com.github.mall.common.dao;

import com.github.mall.common.entity.UmsAdminPermissionRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限)
 * 
 * @author bluesmi
 * @email mi961212y@gmail.com
 * @date 2019-04-22 11:29:36
 */
@Mapper
public interface UmsAdminPermissionRelationDao extends BaseMapper<UmsAdminPermissionRelationEntity> {
	
}
