package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 首页推荐品牌表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_home_brand")
@ApiModel("首页推荐品牌表")
public class SmsHomeBrandEntity extends Model<SmsHomeBrandEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "brand_id")
    @ApiModelProperty(name = "brandId", value = "")
    private String brandId;
    /**
     *
     */
    @TableField(value = "brand_name")
    @ApiModelProperty(name = "brandName", value = "")
    private String brandName;
    /**
     *
     */
    @TableField(value = "recommend_status")
    @ApiModelProperty(name = "recommendStatus", value = "")
    private Integer recommendStatus;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;

}
