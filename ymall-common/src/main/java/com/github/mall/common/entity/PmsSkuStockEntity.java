package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * sku的库存
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_sku_stock")
@ApiModel("sku的库存")
public class PmsSkuStockEntity extends Model<PmsSkuStockEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     * sku编码
     */
    @TableField(value = "sku_code")
    @ApiModelProperty(name = "skuCode", value = "sku编码")
    private String skuCode;
    /**
     *
     */
    @TableField(value = "price")
    @ApiModelProperty(name = "price", value = "")
    private BigDecimal price;
    /**
     * 库存
     */
    @TableField(value = "stock")
    @ApiModelProperty(name = "stock", value = "库存")
    private Integer stock;
    /**
     * 预警库存
     */
    @TableField(value = "low_stock")
    @ApiModelProperty(name = "lowStock", value = "预警库存")
    private Integer lowStock;
    /**
     * 销售属性1
     */
    @TableField(value = "sp1")
    @ApiModelProperty(name = "sp1", value = "销售属性1")
    private String sp1;
    /**
     *
     */
    @TableField(value = "sp2")
    @ApiModelProperty(name = "sp2", value = "")
    private String sp2;
    /**
     *
     */
    @TableField(value = "sp3")
    @ApiModelProperty(name = "sp3", value = "")
    private String sp3;
    /**
     * 展示图片
     */
    @TableField(value = "pic")
    @ApiModelProperty(name = "pic", value = "展示图片")
    private String pic;
    /**
     * 销量
     */
    @TableField(value = "sale")
    @ApiModelProperty(name = "sale", value = "销量")
    private Integer sale;
    /**
     * 单品促销价格
     */
    @TableField(value = "promotion_price")
    @ApiModelProperty(name = "promotionPrice", value = "单品促销价格")
    private BigDecimal promotionPrice;
    /**
     * 锁定库存
     */
    @TableField(value = "lock_stock")
    @ApiModelProperty(name = "lockStock", value = "锁定库存")
    private Integer lockStock;

}
