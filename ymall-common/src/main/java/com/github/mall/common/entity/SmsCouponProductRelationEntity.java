package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 优惠券和产品的关系表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_coupon_product_relation")
@ApiModel("优惠券和产品的关系表")
public class SmsCouponProductRelationEntity extends Model<SmsCouponProductRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "coupon_id")
    @ApiModelProperty(name = "couponId", value = "")
    private String couponId;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "商品名称")
    private String productName;
    /**
     * 商品编码
     */
    @TableField(value = "product_sn")
    @ApiModelProperty(name = "productSn", value = "商品编码")
    private String productSn;

}
