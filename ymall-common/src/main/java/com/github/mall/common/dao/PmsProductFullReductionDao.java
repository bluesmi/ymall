package com.github.mall.common.dao;

import com.github.mall.common.entity.PmsProductFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品满减表(只针对同商品)
 * 
 * @author bluesmi
 * @email mi961212y@gmail.com
 * @date 2019-04-22 11:29:36
 */
@Mapper
public interface PmsProductFullReductionDao extends BaseMapper<PmsProductFullReductionEntity> {
	
}
