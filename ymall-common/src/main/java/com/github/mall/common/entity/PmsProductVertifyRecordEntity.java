package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 商品审核记录
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_vertify_record")
@ApiModel("商品审核记录")
public class PmsProductVertifyRecordEntity extends Model<PmsProductVertifyRecordEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     * 审核人
     */
    @TableField(value = "vertify_man")
    @ApiModelProperty(name = "vertifyMan", value = "审核人")
    private String vertifyMan;
    /**
     *
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "")
    private Integer status;
    /**
     * 反馈详情
     */
    @TableField(value = "detail")
    @ApiModelProperty(name = "detail", value = "反馈详情")
    private String detail;

}
