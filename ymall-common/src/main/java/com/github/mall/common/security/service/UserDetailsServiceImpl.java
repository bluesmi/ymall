package com.github.mall.common.security.service;

import com.github.mall.common.config.SpringContextHolder;
import com.github.mall.common.entity.UmsAdminEntity;
import com.github.mall.common.entity.UmsPermissionEntity;
import com.github.mall.common.security.bo.AdminUserDetails;
import com.github.mall.common.services.UmsAdminService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UmsAdminService umsAdminService = SpringContextHolder.getBean(UmsAdminService.class);
        //获取登录用户信息
        UmsAdminEntity admin = umsAdminService.getAdminByUsername(username);
        if (admin != null) {
            List<UmsPermissionEntity> permissionList = umsAdminService.getPermissionList(admin.getId());
            return new AdminUserDetails(admin,permissionList);
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    }
}
