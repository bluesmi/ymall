package com.github.mall.common.dao;

import com.github.mall.common.entity.PmsProductAttributeValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 存储产品参数信息的表
 * 
 * @author bluesmi
 * @email mi961212y@gmail.com
 * @date 2019-04-22 11:29:36
 */
@Mapper
public interface PmsProductAttributeValueDao extends BaseMapper<PmsProductAttributeValueEntity> {
	
}
