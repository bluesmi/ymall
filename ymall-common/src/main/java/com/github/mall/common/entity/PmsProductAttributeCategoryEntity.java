package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品属性分类表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_attribute_category")
@ApiModel("产品属性分类表")
public class PmsProductAttributeCategoryEntity extends Model<PmsProductAttributeCategoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 属性数量
     */
    @TableField(value = "attribute_count")
    @ApiModelProperty(name = "attributeCount", value = "属性数量")
    private Integer attributeCount;
    /**
     * 参数数量
     */
    @TableField(value = "param_count")
    @ApiModelProperty(name = "paramCount", value = "参数数量")
    private Integer paramCount;

}
