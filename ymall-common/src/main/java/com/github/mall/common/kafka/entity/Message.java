package com.github.mall.common.kafka.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Message<T> implements Serializable {

    private String id;

    private T data;

    private Timestamp startDate;



}
