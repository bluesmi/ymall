package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 会员等级表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_level")
@ApiModel("会员等级表")
public class UmsMemberLevelEntity extends Model<UmsMemberLevelEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     *
     */
    @TableField(value = "growth_point")
    @ApiModelProperty(name = "growthPoint", value = "")
    private Integer growthPoint;
    /**
     * 是否为默认等级：0->不是；1->是
     */
    @TableField(value = "default_status")
    @ApiModelProperty(name = "defaultStatus", value = "是否为默认等级：0->不是；1->是")
    private Integer defaultStatus;
    /**
     * 免运费标准
     */
    @TableField(value = "free_freight_point")
    @ApiModelProperty(name = "freeFreightPoint", value = "免运费标准")
    private BigDecimal freeFreightPoint;
    /**
     * 每次评价获取的成长值
     */
    @TableField(value = "comment_growth_point")
    @ApiModelProperty(name = "commentGrowthPoint", value = "每次评价获取的成长值")
    private Integer commentGrowthPoint;
    /**
     * 是否有免邮特权
     */
    @TableField(value = "priviledge_free_freight")
    @ApiModelProperty(name = "priviledgeFreeFreight", value = "是否有免邮特权")
    private Integer priviledgeFreeFreight;
    /**
     * 是否有签到特权
     */
    @TableField(value = "priviledge_sign_in")
    @ApiModelProperty(name = "priviledgeSignIn", value = "是否有签到特权")
    private Integer priviledgeSignIn;
    /**
     * 是否有评论获奖励特权
     */
    @TableField(value = "priviledge_comment")
    @ApiModelProperty(name = "priviledgeComment", value = "是否有评论获奖励特权")
    private Integer priviledgeComment;
    /**
     * 是否有专享活动特权
     */
    @TableField(value = "priviledge_promotion")
    @ApiModelProperty(name = "priviledgePromotion", value = "是否有专享活动特权")
    private Integer priviledgePromotion;
    /**
     * 是否有会员价格特权
     */
    @TableField(value = "priviledge_member_price")
    @ApiModelProperty(name = "priviledgeMemberPrice", value = "是否有会员价格特权")
    private Integer priviledgeMemberPrice;
    /**
     * 是否有生日特权
     */
    @TableField(value = "priviledge_birthday")
    @ApiModelProperty(name = "priviledgeBirthday", value = "是否有生日特权")
    private Integer priviledgeBirthday;
    /**
     *
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "")
    private String note;

}
