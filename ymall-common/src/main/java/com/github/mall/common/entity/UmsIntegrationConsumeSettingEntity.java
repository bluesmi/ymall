package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 积分消费设置
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_integration_consume_setting")
@ApiModel("积分消费设置")
public class UmsIntegrationConsumeSettingEntity extends Model<UmsIntegrationConsumeSettingEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 每一元需要抵扣的积分数量
     */
    @TableField(value = "deduction_per_amount")
    @ApiModelProperty(name = "deductionPerAmount", value = "每一元需要抵扣的积分数量")
    private Integer deductionPerAmount;
    /**
     * 每笔订单最高抵用百分比
     */
    @TableField(value = "max_percent_per_order")
    @ApiModelProperty(name = "maxPercentPerOrder", value = "每笔订单最高抵用百分比")
    private Integer maxPercentPerOrder;
    /**
     * 每次使用积分最小单位100
     */
    @TableField(value = "use_unit")
    @ApiModelProperty(name = "useUnit", value = "每次使用积分最小单位100")
    private Integer useUnit;
    /**
     * 是否可以和优惠券同用；0->不可以；1->可以
     */
    @TableField(value = "coupon_status")
    @ApiModelProperty(name = "couponStatus", value = "是否可以和优惠券同用；0->不可以；1->可以")
    private Integer couponStatus;

}
