package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 后台用户登录日志表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_admin_login_log")
@ApiModel("后台用户登录日志表")
public class UmsAdminLoginLogEntity extends Model<UmsAdminLoginLogEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "admin_id")
    @ApiModelProperty(name = "adminId", value = "")
    private String adminId;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "ip")
    @ApiModelProperty(name = "ip", value = "")
    private String ip;
    /**
     *
     */
    @TableField(value = "address")
    @ApiModelProperty(name = "address", value = "")
    private String address;
    /**
     * 浏览器登录类型
     */
    @TableField(value = "user_agent")
    @ApiModelProperty(name = "userAgent", value = "浏览器登录类型")
    private String userAgent;

}
