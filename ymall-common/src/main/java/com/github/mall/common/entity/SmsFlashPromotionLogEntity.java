package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 限时购通知记录
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_flash_promotion_log")
@ApiModel("限时购通知记录")
public class SmsFlashPromotionLogEntity extends Model<SmsFlashPromotionLogEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private Integer id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private Integer memberId;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "member_phone")
    @ApiModelProperty(name = "memberPhone", value = "")
    private String memberPhone;
    /**
     *
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "")
    private String productName;
    /**
     * 会员订阅时间
     */
    @TableField(value = "subscribe_time")
    @ApiModelProperty(name = "subscribeTime", value = "会员订阅时间")
    private Date subscribeTime;
    /**
     *
     */
    @TableField(value = "send_time")
    @ApiModelProperty(name = "sendTime", value = "")
    private Date sendTime;

}
