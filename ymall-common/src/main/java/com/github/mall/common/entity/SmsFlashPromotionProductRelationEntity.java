package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 商品限时购与商品关系表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_flash_promotion_product_relation")
@ApiModel("商品限时购与商品关系表")
public class SmsFlashPromotionProductRelationEntity extends Model<SmsFlashPromotionProductRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "编号")
    private String id;
    /**
     *
     */
    @TableField(value = "flash_promotion_id")
    @ApiModelProperty(name = "flashPromotionId", value = "")
    private String flashPromotionId;
    /**
     * 编号
     */
    @TableField(value = "flash_promotion_session_id")
    @ApiModelProperty(name = "flashPromotionSessionId", value = "编号")
    private String flashPromotionSessionId;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     * 限时购价格
     */
    @TableField(value = "flash_promotion_price")
    @ApiModelProperty(name = "flashPromotionPrice", value = "限时购价格")
    private BigDecimal flashPromotionPrice;
    /**
     * 限时购数量
     */
    @TableField(value = "flash_promotion_count")
    @ApiModelProperty(name = "flashPromotionCount", value = "限时购数量")
    private Integer flashPromotionCount;
    /**
     * 每人限购数量
     */
    @TableField(value = "flash_promotion_limit")
    @ApiModelProperty(name = "flashPromotionLimit", value = "每人限购数量")
    private Integer flashPromotionLimit;
    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "排序")
    private Integer sort;

}
