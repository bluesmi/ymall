package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品分类
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_category")
@ApiModel("产品分类")
public class PmsProductCategoryEntity extends Model<PmsProductCategoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 上机分类的编号：0表示一级分类
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(name = "parentId", value = "上机分类的编号：0表示一级分类")
    private String parentId;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 分类级别：0->1级；1->2级
     */
    @TableField(value = "level")
    @ApiModelProperty(name = "level", value = "分类级别：0->1级；1->2级")
    private Integer level;
    /**
     *
     */
    @TableField(value = "product_count")
    @ApiModelProperty(name = "productCount", value = "")
    private Integer productCount;
    /**
     *
     */
    @TableField(value = "product_unit")
    @ApiModelProperty(name = "productUnit", value = "")
    private String productUnit;
    /**
     * 是否显示在导航栏：0->不显示；1->显示
     */
    @TableField(value = "nav_status")
    @ApiModelProperty(name = "navStatus", value = "是否显示在导航栏：0->不显示；1->显示")
    private Integer navStatus;
    /**
     * 显示状态：0->不显示；1->显示
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "显示状态：0->不显示；1->显示")
    private Integer showStatus;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;
    /**
     * 图标
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "图标")
    private String icon;
    /**
     *
     */
    @TableField(value = "keywords")
    @ApiModelProperty(name = "keywords", value = "")
    private String keywords;
    /**
     * 描述
     */
    @TableField(value = "description")
    @ApiModelProperty(name = "description", value = "描述")
    private String description;

}
