package com.github.mall.common.kafka.util;

import com.github.mall.common.config.SpringContextHolder;
import com.github.mall.common.kafka.provider.KafkaProvider;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class KafkaConsumerUtil {


    public static void sendMessage(String topic, Object data){
        SpringContextHolder.getBean(KafkaProvider.class).send(topic,data);
//        kafkaProvider.send(topic, data);
    }
}
