package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 话题表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_topic")
@ApiModel("话题表")
public class CmsTopicEntity extends Model<CmsTopicEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "category_id")
    @ApiModelProperty(name = "categoryId", value = "")
    private String categoryId;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "start_time")
    @ApiModelProperty(name = "startTime", value = "")
    private Date startTime;
    /**
     *
     */
    @TableField(value = "end_time")
    @ApiModelProperty(name = "endTime", value = "")
    private Date endTime;
    /**
     * 参与人数
     */
    @TableField(value = "attend_count")
    @ApiModelProperty(name = "attendCount", value = "参与人数")
    private Integer attendCount;
    /**
     * 关注人数
     */
    @TableField(value = "attention_count")
    @ApiModelProperty(name = "attentionCount", value = "关注人数")
    private Integer attentionCount;
    /**
     *
     */
    @TableField(value = "read_count")
    @ApiModelProperty(name = "readCount", value = "")
    private Integer readCount;
    /**
     * 奖品名称
     */
    @TableField(value = "award_name")
    @ApiModelProperty(name = "awardName", value = "奖品名称")
    private String awardName;
    /**
     * 参与方式
     */
    @TableField(value = "attend_type")
    @ApiModelProperty(name = "attendType", value = "参与方式")
    private String attendType;
    /**
     * 话题内容
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "话题内容")
    private String content;

}
