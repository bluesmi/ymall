package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 订单操作历史记录
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order_operate_history")
@ApiModel("订单操作历史记录")
public class OmsOrderOperateHistoryEntity extends Model<OmsOrderOperateHistoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 订单id
     */
    @TableField(value = "order_id")
    @ApiModelProperty(name = "orderId", value = "订单id")
    private String orderId;
    /**
     * 操作人：用户；系统；后台管理员
     */
    @TableField(value = "operate_man")
    @ApiModelProperty(name = "operateMan", value = "操作人：用户；系统；后台管理员")
    private String operateMan;
    /**
     * 操作时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "操作时间")
    private Date createTime;
    /**
     * 订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
     */
    @TableField(value = "order_status")
    @ApiModelProperty(name = "orderStatus", value = "订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单")
    private Integer orderStatus;
    /**
     * 备注
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "备注")
    private String note;

}
