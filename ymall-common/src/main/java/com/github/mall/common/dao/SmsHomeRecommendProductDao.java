package com.github.mall.common.dao;

import com.github.mall.common.entity.SmsHomeRecommendProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人气推荐商品表
 * 
 * @author bluesmi
 * @email mi961212y@gmail.com
 * @date 2019-04-22 11:29:36
 */
@Mapper
public interface SmsHomeRecommendProductDao extends BaseMapper<SmsHomeRecommendProductEntity> {
	
}
