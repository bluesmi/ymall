package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 限时购场次表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_flash_promotion_session")
@ApiModel("限时购场次表")
public class SmsFlashPromotionSessionEntity extends Model<SmsFlashPromotionSessionEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "编号")
    private String id;
    /**
     * 场次名称
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "场次名称")
    private String name;
    /**
     * 每日开始时间
     */
    @TableField(value = "start_time")
    @ApiModelProperty(name = "startTime", value = "每日开始时间")
    private Timestamp startTime;
    /**
     * 每日结束时间
     */
    @TableField(value = "end_time")
    @ApiModelProperty(name = "endTime", value = "每日结束时间")
    private Timestamp endTime;
    /**
     * 启用状态：0->不启用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "启用状态：0->不启用；1->启用")
    private Integer status;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Date createTime;

}
