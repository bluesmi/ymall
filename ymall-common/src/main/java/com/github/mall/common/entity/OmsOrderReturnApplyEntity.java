package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单退货申请
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order_return_apply")
@ApiModel("订单退货申请")
public class OmsOrderReturnApplyEntity extends Model<OmsOrderReturnApplyEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 订单id
     */
    @TableField(value = "order_id")
    @ApiModelProperty(name = "orderId", value = "订单id")
    private String orderId;
    /**
     * 收货地址表id
     */
    @TableField(value = "company_address_id")
    @ApiModelProperty(name = "companyAddressId", value = "收货地址表id")
    private String companyAddressId;
    /**
     * 退货商品id
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "退货商品id")
    private String productId;
    /**
     * 订单编号
     */
    @TableField(value = "order_sn")
    @ApiModelProperty(name = "orderSn", value = "订单编号")
    private String orderSn;
    /**
     * 申请时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "申请时间")
    private Date createTime;
    /**
     * 会员用户名
     */
    @TableField(value = "member_username")
    @ApiModelProperty(name = "memberUsername", value = "会员用户名")
    private String memberUsername;
    /**
     * 退款金额
     */
    @TableField(value = "return_amount")
    @ApiModelProperty(name = "returnAmount", value = "退款金额")
    private BigDecimal returnAmount;
    /**
     * 退货人姓名
     */
    @TableField(value = "return_name")
    @ApiModelProperty(name = "returnName", value = "退货人姓名")
    private String returnName;
    /**
     * 退货人电话
     */
    @TableField(value = "return_phone")
    @ApiModelProperty(name = "returnPhone", value = "退货人电话")
    private String returnPhone;
    /**
     * 申请状态：0->待处理；1->退货中；2->已完成；3->已拒绝
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "申请状态：0->待处理；1->退货中；2->已完成；3->已拒绝")
    private Integer status;
    /**
     * 处理时间
     */
    @TableField(value = "handle_time")
    @ApiModelProperty(name = "handleTime", value = "处理时间")
    private Date handleTime;
    /**
     * 商品图片
     */
    @TableField(value = "product_pic")
    @ApiModelProperty(name = "productPic", value = "商品图片")
    private String productPic;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "商品名称")
    private String productName;
    /**
     * 商品品牌
     */
    @TableField(value = "product_brand")
    @ApiModelProperty(name = "productBrand", value = "商品品牌")
    private String productBrand;
    /**
     * 商品销售属性：颜色：红色；尺码：xl;
     */
    @TableField(value = "product_attr")
    @ApiModelProperty(name = "productAttr", value = "商品销售属性：颜色：红色；尺码：xl;")
    private String productAttr;
    /**
     * 退货数量
     */
    @TableField(value = "product_count")
    @ApiModelProperty(name = "productCount", value = "退货数量")
    private Integer productCount;
    /**
     * 商品单价
     */
    @TableField(value = "product_price")
    @ApiModelProperty(name = "productPrice", value = "商品单价")
    private BigDecimal productPrice;
    /**
     * 商品实际支付单价
     */
    @TableField(value = "product_real_price")
    @ApiModelProperty(name = "productRealPrice", value = "商品实际支付单价")
    private BigDecimal productRealPrice;
    /**
     * 原因
     */
    @TableField(value = "reason")
    @ApiModelProperty(name = "reason", value = "原因")
    private String reason;
    /**
     * 描述
     */
    @TableField(value = "description")
    @ApiModelProperty(name = "description", value = "描述")
    private String description;
    /**
     * 凭证图片，以逗号隔开
     */
    @TableField(value = "proof_pics")
    @ApiModelProperty(name = "proofPics", value = "凭证图片，以逗号隔开")
    private String proofPics;
    /**
     * 处理备注
     */
    @TableField(value = "handle_note")
    @ApiModelProperty(name = "handleNote", value = "处理备注")
    private String handleNote;
    /**
     * 处理人员
     */
    @TableField(value = "handle_man")
    @ApiModelProperty(name = "handleMan", value = "处理人员")
    private String handleMan;
    /**
     * 收货人
     */
    @TableField(value = "receive_man")
    @ApiModelProperty(name = "receiveMan", value = "收货人")
    private String receiveMan;
    /**
     * 收货时间
     */
    @TableField(value = "receive_time")
    @ApiModelProperty(name = "receiveTime", value = "收货时间")
    private Date receiveTime;
    /**
     * 收货备注
     */
    @TableField(value = "receive_note")
    @ApiModelProperty(name = "receiveNote", value = "收货备注")
    private String receiveNote;

}
