package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 会员与产品分类关系表（用户喜欢的分类）
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_product_category_relation")
@ApiModel("会员与产品分类关系表（用户喜欢的分类）")
public class UmsMemberProductCategoryRelationEntity extends Model<UmsMemberProductCategoryRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     *
     */
    @TableField(value = "product_category_id")
    @ApiModelProperty(name = "productCategoryId", value = "")
    private String productCategoryId;

}
