package com.github.mall.common.services;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.mall.common.entity.UmsPermissionEntity;
import com.github.mall.common.entity.UmsRoleEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

public interface UmsPermissionService  extends IService<UmsPermissionEntity> {

    @ApiOperation("根据角色标识查询用户权限")
    @ApiParam(name = "userId",value = "用户名")
    List<UmsPermissionEntity> getUserPermissionList(String roleId);

    UmsPermissionEntity addPermission(Dict permissionParam);

    boolean checkPermissionIsExist(Dict param);
    void addRolePermissionRalation(UmsRoleEntity umsRoleEntity, List<String> list);
}
