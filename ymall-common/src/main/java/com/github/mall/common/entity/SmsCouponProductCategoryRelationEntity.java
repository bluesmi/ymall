package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 优惠券和产品分类关系表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_coupon_product_category_relation")
@ApiModel("优惠券和产品分类关系表")
public class SmsCouponProductCategoryRelationEntity extends Model<SmsCouponProductCategoryRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "coupon_id")
    @ApiModelProperty(name = "couponId", value = "")
    private String couponId;
    /**
     *
     */
    @TableField(value = "product_category_id")
    @ApiModelProperty(name = "productCategoryId", value = "")
    private String productCategoryId;
    /**
     * 产品分类名称
     */
    @TableField(value = "product_category_name")
    @ApiModelProperty(name = "productCategoryName", value = "产品分类名称")
    private String productCategoryName;
    /**
     * 父分类名称
     */
    @TableField(value = "parent_category_name")
    @ApiModelProperty(name = "parentCategoryName", value = "父分类名称")
    private String parentCategoryName;

}
