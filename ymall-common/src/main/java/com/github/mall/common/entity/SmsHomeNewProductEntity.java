package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 新鲜好物表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_home_new_product")
@ApiModel("新鲜好物表")
public class SmsHomeNewProductEntity extends Model<SmsHomeNewProductEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "")
    private String productName;
    /**
     *
     */
    @TableField(value = "recommend_status")
    @ApiModelProperty(name = "recommendStatus", value = "")
    private Integer recommendStatus;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;

}
