package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 首页推荐专题表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_home_recommend_subject")
@ApiModel("首页推荐专题表")
public class SmsHomeRecommendSubjectEntity extends Model<SmsHomeRecommendSubjectEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "subject_id")
    @ApiModelProperty(name = "subjectId", value = "")
    private String subjectId;
    /**
     *
     */
    @TableField(value = "subject_name")
    @ApiModelProperty(name = "subjectName", value = "")
    private String subjectName;
    /**
     *
     */
    @TableField(value = "recommend_status")
    @ApiModelProperty(name = "recommendStatus", value = "")
    private Integer recommendStatus;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;

}
