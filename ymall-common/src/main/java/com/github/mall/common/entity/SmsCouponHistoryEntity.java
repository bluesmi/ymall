package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 优惠券使用、领取历史表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_coupon_history")
@ApiModel("优惠券使用、领取历史表")
public class SmsCouponHistoryEntity extends Model<SmsCouponHistoryEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "coupon_id")
    @ApiModelProperty(name = "couponId", value = "")
    private String couponId;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     *
     */
    @TableField(value = "coupon_code")
    @ApiModelProperty(name = "couponCode", value = "")
    private String couponCode;
    /**
     * 领取人昵称
     */
    @TableField(value = "member_nickname")
    @ApiModelProperty(name = "memberNickname", value = "领取人昵称")
    private String memberNickname;
    /**
     * 获取类型：0->后台赠送；1->主动获取
     */
    @TableField(value = "get_type")
    @ApiModelProperty(name = "getType", value = "获取类型：0->后台赠送；1->主动获取")
    private Integer getType;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     * 使用状态：0->未使用；1->已使用；2->已过期
     */
    @TableField(value = "use_status")
    @ApiModelProperty(name = "useStatus", value = "使用状态：0->未使用；1->已使用；2->已过期")
    private Integer useStatus;
    /**
     * 使用时间
     */
    @TableField(value = "use_time")
    @ApiModelProperty(name = "useTime", value = "使用时间")
    private Date useTime;
    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    @ApiModelProperty(name = "orderId", value = "订单编号")
    private String orderId;
    /**
     * 订单号码
     */
    @TableField(value = "order_sn")
    @ApiModelProperty(name = "orderSn", value = "订单号码")
    private String orderSn;

}
