package com.github.mall.common.util;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ReflectUtil;
import lombok.SneakyThrows;

import java.lang.reflect.Field;


public class EntityUtils {
    /**
     * Map转实体类
     *
     * @param map    需要初始化的数据，key字段必须与实体类的成员名字一样，否则赋值为空
     * @param entity 需要转化成的实体类
     * @return
     */
    @SneakyThrows
    public static <T> T mapToEntity(Dict map, Class<T> entity){
        T t = ReflectUtil.newInstance(entity);
        for (Field field : entity.getDeclaredFields()) {
            if (map.containsKey(field.getName())) {
                boolean flag = field.canAccess(t);
                field.setAccessible(true);
                Object object = map.get(field.getName());
                if (object != null && field.getType().isAssignableFrom(object.getClass())) {
                    field.set(t, object);
                }
                field.setAccessible(flag);
            }
        }
        return t;
    }

}
