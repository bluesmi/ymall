package com.github.mall.common.constant.enmus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KafkaTopicEnums {
    SYS_LOG_TOPIC("sys_log_topic"),
    UMS_MAIL_TOPIC("ums_mail_topic");
    private final  String topic;
}
