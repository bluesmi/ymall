package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 后台用户权限表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_permission")
@ApiModel("后台用户权限表")
public class UmsPermissionEntity extends Model<UmsPermissionEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 父级权限id
     */
    @TableField(value = "pid")
    @ApiModelProperty(name = "pid", value = "父级权限id")
    private String pid;
    /**
     * 名称
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "名称")
    private String name;
    /**
     * 权限值
     */
    @TableField(value = "value")
    @ApiModelProperty(name = "value", value = "权限值")
    private String value;
    /**
     * 图标
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "图标")
    private String icon;
    /**
     * 权限类型：0->目录；1->菜单；2->按钮（接口绑定权限）
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "权限类型：0->目录；1->菜单；2->按钮（接口绑定权限）")
    private Integer type;
    /**
     * 前端资源路径
     */
    @TableField(value = "uri")
    @ApiModelProperty(name = "uri", value = "前端资源路径")
    private String uri;
    /**
     * 启用状态；0->禁用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "启用状态；0->禁用；1->启用")
    private Integer status;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Date createTime;
    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "排序")
    private Integer sort;

}
