package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户举报表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_member_report")
@ApiModel("用户举报表")
public class CmsMemberReportEntity extends Model<CmsMemberReportEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 举报类型：0->商品评价；1->话题内容；2->用户评论
     */
    @TableField(value = "report_type")
    @ApiModelProperty(name = "reportType", value = "举报类型：0->商品评价；1->话题内容；2->用户评论")
    private Integer reportType;
    /**
     * 举报人
     */
    @TableField(value = "report_member_name")
    @ApiModelProperty(name = "reportMemberName", value = "举报人")
    private String reportMemberName;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "report_object")
    @ApiModelProperty(name = "reportObject", value = "")
    private String reportObject;
    /**
     * 举报状态：0->未处理；1->已处理
     */
    @TableField(value = "report_status")
    @ApiModelProperty(name = "reportStatus", value = "举报状态：0->未处理；1->已处理")
    private Integer reportStatus;
    /**
     * 处理结果：0->无效；1->有效；2->恶意
     */
    @TableField(value = "handle_status")
    @ApiModelProperty(name = "handleStatus", value = "处理结果：0->无效；1->有效；2->恶意")
    private Integer handleStatus;
    /**
     *
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "")
    private String note;

}
