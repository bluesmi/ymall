package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 订单设置表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_order_setting")
@ApiModel("订单设置表")
public class OmsOrderSettingEntity extends Model<OmsOrderSettingEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     * 秒杀订单超时关闭时间(分)
     */
    @TableField(value = "flash_order_overtime")
    @ApiModelProperty(name = "flashOrderOvertime", value = "秒杀订单超时关闭时间(分)")
    private Integer flashOrderOvertime;
    /**
     * 正常订单超时时间(分)
     */
    @TableField(value = "normal_order_overtime")
    @ApiModelProperty(name = "normalOrderOvertime", value = "正常订单超时时间(分)")
    private Integer normalOrderOvertime;
    /**
     * 发货后自动确认收货时间（天）
     */
    @TableField(value = "confirm_overtime")
    @ApiModelProperty(name = "confirmOvertime", value = "发货后自动确认收货时间（天）")
    private Integer confirmOvertime;
    /**
     * 自动完成交易时间，不能申请售后（天）
     */
    @TableField(value = "finish_overtime")
    @ApiModelProperty(name = "finishOvertime", value = "自动完成交易时间，不能申请售后（天）")
    private Integer finishOvertime;
    /**
     * 订单完成后自动好评时间（天）
     */
    @TableField(value = "comment_overtime")
    @ApiModelProperty(name = "commentOvertime", value = "订单完成后自动好评时间（天）")
    private Integer commentOvertime;

}
