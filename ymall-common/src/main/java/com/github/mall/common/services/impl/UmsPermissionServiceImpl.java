package com.github.mall.common.services.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.mall.common.dao.UmsPermissionDao;
import com.github.mall.common.dao.UmsRolePermissionRelationDao;
import com.github.mall.common.entity.UmsPermissionEntity;
import com.github.mall.common.entity.UmsRoleEntity;
import com.github.mall.common.entity.UmsRolePermissionRelationEntity;
import com.github.mall.common.services.UmsPermissionService;
import com.github.mall.common.util.EntityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UmsPermissionServiceImpl extends ServiceImpl<UmsPermissionDao, UmsPermissionEntity> implements UmsPermissionService {

    private final UmsRolePermissionRelationDao umsRolePermissionRelationDao;
    private final UmsPermissionDao umsPermissionDao;

    @Override
    public List<UmsPermissionEntity> getUserPermissionList(String roleId) {
        UmsRolePermissionRelationEntity umsRolePermissionRelationEntity = new UmsRolePermissionRelationEntity();
        umsRolePermissionRelationEntity.setRoleId(roleId);
        return umsRolePermissionRelationDao
                .selectList(new QueryWrapper<>(umsRolePermissionRelationEntity))
                .stream()
                .map(e -> umsPermissionDao.selectById(e.getPermissionId()))
                .collect(Collectors.toList());
    }

    @Override
    public UmsPermissionEntity addPermission(Dict permissionParam) {
        //根据权限名称查询权限是否存在,根据父级权限ID，权限名，权限值去查
        if (checkPermissionIsExist(permissionParam)) {
            UmsPermissionEntity umsPermissionEntity = EntityUtils.mapToEntity(permissionParam, UmsPermissionEntity.class);
            if (umsPermissionEntity.getCreateTime() == null) {
                umsPermissionEntity.setCreateTime(DateUtil.date());
            }
            int flag = umsPermissionDao.insert(umsPermissionEntity);
            if (flag != 0) {
                return umsPermissionEntity;
            }
        }

        return null;
    }

    @Override
    public boolean checkPermissionIsExist(Dict param) {
        Dict checkParam = new Dict(param);
        if (checkParam.containsKey("icon")) {
            checkParam.remove("icon");
        }
        if (checkParam.containsKey("status")) {
            checkParam.remove("status");
        }
        if (checkParam.containsKey("sort")) {
            checkParam.remove("sort");
        }
        List umsPermissionEntityList = umsPermissionDao.selectByMap(checkParam);
        return umsPermissionEntityList.size() == 0 ? true : false;
    }

    @Override
    public void addRolePermissionRalation(UmsRoleEntity umsRoleEntity, List<String> list) {
        list.stream()
                .map(e -> new UmsRolePermissionRelationEntity(null, umsRoleEntity.getId(), e))
                .collect(Collectors.toList())
                .forEach(e -> umsRolePermissionRelationDao.insert(e));
        ;

    }


}
