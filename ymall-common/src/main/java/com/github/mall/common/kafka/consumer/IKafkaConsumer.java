package com.github.mall.common.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface IKafkaConsumer {


    public void listen(ConsumerRecord<?, ?> record);
}
