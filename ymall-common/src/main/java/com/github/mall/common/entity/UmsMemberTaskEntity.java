package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 会员任务表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_task")
@ApiModel("会员任务表")
public class UmsMemberTaskEntity extends Model<UmsMemberTaskEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 赠送成长值
     */
    @TableField(value = "growth")
    @ApiModelProperty(name = "growth", value = "赠送成长值")
    private Integer growth;
    /**
     * 赠送积分
     */
    @TableField(value = "intergration")
    @ApiModelProperty(name = "intergration", value = "赠送积分")
    private Integer intergration;
    /**
     * 任务类型：0->新手任务；1->日常任务
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "任务类型：0->新手任务；1->日常任务")
    private Integer type;

}
