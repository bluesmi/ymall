package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 产品阶梯价格表(只针对同商品)
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_ladder")
@ApiModel("产品阶梯价格表(只针对同商品)")
public class PmsProductLadderEntity extends Model<PmsProductLadderEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     * 满足的商品数量
     */
    @TableField(value = "count")
    @ApiModelProperty(name = "count", value = "满足的商品数量")
    private Integer count;
    /**
     * 折扣
     */
    @TableField(value = "discount")
    @ApiModelProperty(name = "discount", value = "折扣")
    private BigDecimal discount;
    /**
     * 折后价格
     */
    @TableField(value = "price")
    @ApiModelProperty(name = "price", value = "折后价格")
    private BigDecimal price;

}
