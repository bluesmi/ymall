package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品属性参数表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_attribute")
@ApiModel("商品属性参数表")
public class PmsProductAttributeEntity extends Model<PmsProductAttributeEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_attribute_category_id")
    @ApiModelProperty(name = "productAttributeCategoryId", value = "")
    private String productAttributeCategoryId;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 属性选择类型：0->唯一；1->单选；2->多选
     */
    @TableField(value = "select_type")
    @ApiModelProperty(name = "selectType", value = "属性选择类型：0->唯一；1->单选；2->多选")
    private Integer selectType;
    /**
     * 属性录入方式：0->手工录入；1->从列表中选取
     */
    @TableField(value = "input_type")
    @ApiModelProperty(name = "inputType", value = "属性录入方式：0->手工录入；1->从列表中选取")
    private Integer inputType;
    /**
     * 可选值列表，以逗号隔开
     */
    @TableField(value = "input_list")
    @ApiModelProperty(name = "inputList", value = "可选值列表，以逗号隔开")
    private String inputList;
    /**
     * 排序字段：最高的可以单独上传图片
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "排序字段：最高的可以单独上传图片")
    private Integer sort;
    /**
     * 分类筛选样式：1->普通；1->颜色
     */
    @TableField(value = "filter_type")
    @ApiModelProperty(name = "filterType", value = "分类筛选样式：1->普通；1->颜色")
    private Integer filterType;
    /**
     * 检索类型；0->不需要进行检索；1->关键字检索；2->范围检索
     */
    @TableField(value = "search_type")
    @ApiModelProperty(name = "searchType", value = "检索类型；0->不需要进行检索；1->关键字检索；2->范围检索")
    private Integer searchType;
    /**
     * 相同属性产品是否关联；0->不关联；1->关联
     */
    @TableField(value = "related_status")
    @ApiModelProperty(name = "relatedStatus", value = "相同属性产品是否关联；0->不关联；1->关联")
    private Integer relatedStatus;
    /**
     * 是否支持手动新增；0->不支持；1->支持
     */
    @TableField(value = "hand_add_status")
    @ApiModelProperty(name = "handAddStatus", value = "是否支持手动新增；0->不支持；1->支持")
    private Integer handAddStatus;
    /**
     * 属性的类型；0->规格；1->参数
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "属性的类型；0->规格；1->参数")
    private Integer type;

}
