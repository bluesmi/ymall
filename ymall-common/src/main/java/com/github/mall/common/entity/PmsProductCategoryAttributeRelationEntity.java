package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_category_attribute_relation")
@ApiModel("产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
public class PmsProductCategoryAttributeRelationEntity extends Model<PmsProductCategoryAttributeRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_category_id")
    @ApiModelProperty(name = "productCategoryId", value = "")
    private String productCategoryId;
    /**
     *
     */
    @TableField(value = "product_attribute_id")
    @ApiModelProperty(name = "productAttributeId", value = "")
    private String productAttributeId;

}
