package com.github.mall.common.services;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.mall.common.entity.UmsRoleEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

public interface UmsRoleService  extends IService<UmsRoleEntity> {

    @ApiOperation("根据用户标识查询用户角色")
    @ApiParam(name = "userId",value = "用户名")
    List<UmsRoleEntity> getUserRoleList(String userId);

    UmsRoleEntity addRole(Dict roleParam);

    public boolean checkRoleIsExist(Dict roleParam);


}
