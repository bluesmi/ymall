package com.github.mall.common.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.mall.common.dao.SysLogDao;
import com.github.mall.common.entity.SysLogEntity;
import com.github.mall.common.services.SysLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SysLogServiceImpl   extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {
}
