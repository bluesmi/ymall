package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 帮助分类表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_help_category" )
@ApiModel("帮助分类表")
public class CmsHelpCategoryEntity extends Model<CmsHelpCategoryEntity> {
    private static final long serialVersionUID = 1L;

        /**
     * 
     */
        @TableId(value = "id" , type = IdType.UUID)
        @ApiModelProperty(name="id",value="")
    private String id;
        /**
     * 
     */
        @TableField(value = "name")
        @ApiModelProperty(name="name",value="")
    private String name;
        /**
     * 分类图标
     */
        @TableField(value = "icon")
        @ApiModelProperty(name="icon",value="分类图标")
    private String icon;
        /**
     * 专题数量
     */
        @TableField(value = "help_count")
        @ApiModelProperty(name="helpCount",value="专题数量")
    private Integer helpCount;
        /**
     * 
     */
        @TableField(value = "show_status")
        @ApiModelProperty(name="showStatus",value="")
    private Integer showStatus;
        /**
     * 
     */
        @TableField(value = "sort")
        @ApiModelProperty(name="sort",value="")
    private Integer sort;
    
}
