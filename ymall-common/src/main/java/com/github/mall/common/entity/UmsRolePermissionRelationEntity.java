package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 后台用户角色和权限关系表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_role_permission_relation")
@ApiModel("后台用户角色和权限关系表")
@AllArgsConstructor
@NoArgsConstructor
public class UmsRolePermissionRelationEntity extends Model<UmsRolePermissionRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "role_id")
    @ApiModelProperty(name = "roleId", value = "")
    private String roleId;
    /**
     *
     */
    @TableField(value = "permission_id")
    @ApiModelProperty(name = "permissionId", value = "")
    private String permissionId;

}
