package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 帮助表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_help")
@ApiModel("帮助表")
public class CmsHelpEntity extends Model<CmsHelpEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "category_id")
    @ApiModelProperty(name = "categoryId", value = "")
    private String categoryId;
    /**
     *
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "")
    private String icon;
    /**
     *
     */
    @TableField(value = "title")
    @ApiModelProperty(name = "title", value = "")
    private String title;
    /**
     *
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "")
    private Integer showStatus;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "read_count")
    @ApiModelProperty(name = "readCount", value = "")
    private Integer readCount;
    /**
     *
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "")
    private String content;

}
