package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 后台用户表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_admin")
@ApiModel("后台用户表")
public class UmsAdminEntity extends Model<UmsAdminEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "username")
    @ApiModelProperty(name = "username", value = "")
    private String username;
    /**
     *
     */
    @TableField(value = "password")
    @ApiModelProperty(name = "password", value = "")
    private String password;
    /**
     * 头像
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "头像")
    private String icon;
    /**
     * 邮箱
     */
    @TableField(value = "email")
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    @ApiModelProperty(name = "nickName", value = "昵称")
    private String nickName;
    /**
     * 备注信息
     */
    @TableField(value = "note")
    @ApiModelProperty(name = "note", value = "备注信息")
    private String note;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Date createTime;
    /**
     * 最后登录时间
     */
    @TableField(value = "login_time")
    @ApiModelProperty(name = "loginTime", value = "最后登录时间")
    private Date loginTime;
    /**
     * 帐号启用状态：0->禁用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "帐号启用状态：0->禁用；1->启用")
    private Integer status;

}
