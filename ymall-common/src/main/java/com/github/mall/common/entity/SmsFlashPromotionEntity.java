package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 限时购表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms_flash_promotion")
@ApiModel("限时购表")
public class SmsFlashPromotionEntity extends Model<SmsFlashPromotionEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "title")
    @ApiModelProperty(name = "title", value = "")
    private String title;
    /**
     * 开始日期
     */
    @TableField(value = "start_date")
    @ApiModelProperty(name = "startDate", value = "开始日期")
    private Date startDate;
    /**
     * 结束日期
     */
    @TableField(value = "end_date")
    @ApiModelProperty(name = "endDate", value = "结束日期")
    private Date endDate;
    /**
     * 上下线状态
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "上下线状态")
    private Integer status;
    /**
     * 秒杀时间段名称
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "秒杀时间段名称")
    private Date createTime;

}
