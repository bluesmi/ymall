package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 存储产品参数信息的表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_product_attribute_value")
@ApiModel("存储产品参数信息的表")
public class PmsProductAttributeValueEntity extends Model<PmsProductAttributeValueEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "product_attribute_id")
    @ApiModelProperty(name = "productAttributeId", value = "")
    private String productAttributeId;
    /**
     * 手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开
     */
    @TableField(value = "value")
    @ApiModelProperty(name = "value", value = "手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开")
    private String value;

}
