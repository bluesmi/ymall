package com.github.mall.common.constant.enmus;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态返回码定义
 */
@Getter
@AllArgsConstructor
public enum CommontReturnEnums {


  OK(200, "服务器成功返回请求的数据"),
  CREATED(201, "新建或修改数据成功"),
  ACCEPTED(202, "一个请求已经进入后台排队（异步任务）"),
  NO_CONTENT(204, "删除数据成功"),
  BAD_REQUEST(400, "发出的请求有错误，服务器没有进行新建或修改数据的操作"),
  UNAUTHORIZED(401, "用户没有权限（令牌、用户名、密码错误"),
  FORBIDDEN(403, "用户得到授权，但是访问是被禁止的"),
  NOT_FOUND(404, "发出的请求针对的是不存在的记录，服务器没有进行操作"),
  NOT_ACCEPTABLE(406, "请求的格式不可得"),
  NOT_CREATED(409, "增加或修改失败"),
  GONE(410, "请求的资源被永久删除，且不会再得到的"),
  UNPROCESSABLE_ENTITY(422, "当创建一个对象时，发生一个验证错误"),
  AUTH_CODE_WRONG(489, "验证码错误"),
  INTERNAL_SERVER_ERROR(500, "服务器发生错误，请检查服务器"),
  BAD_GATEWAY(502, "网关错误"),
  SERVICE_UNAVAILABLE(503, "服务不可用，服务器暂时过载或维护"),
  GATEWAY_TIMEOUT(504, "网关超时");
  /**
   * 状态码
   */
  private final int code;

  /**
   * 返回信息
   */
  private final String message;


}
