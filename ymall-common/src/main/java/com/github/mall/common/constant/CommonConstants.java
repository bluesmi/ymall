package com.github.mall.common.constant;

public interface CommonConstants {

    /**
     * 服务器返回成功
     */
    Integer SUCCESS = 200;
    /**
     * 服务器内部错误
     */
    Integer FAIL = 500;


    /**
     * 编码
     */
    String UTF8 = "UTF-8";

    /**
     * JSON 资源
     */
    String CONTENT_TYPE = "application/json; charset=utf-8";

    int UMS_ADMIN_STATUS = 1;

}
