package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 用户标签表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member_tag")
@ApiModel("用户标签表")
public class UmsMemberTagEntity extends Model<UmsMemberTagEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 自动打标签完成订单数量
     */
    @TableField(value = "finish_order_count")
    @ApiModelProperty(name = "finishOrderCount", value = "自动打标签完成订单数量")
    private Integer finishOrderCount;
    /**
     * 自动打标签完成订单金额
     */
    @TableField(value = "finish_order_amount")
    @ApiModelProperty(name = "finishOrderAmount", value = "自动打标签完成订单金额")
    private BigDecimal finishOrderAmount;

}
