package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oms_cart_item")
@ApiModel("购物车表")
public class OmsCartItemEntity extends Model<OmsCartItemEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "product_id")
    @ApiModelProperty(name = "productId", value = "")
    private String productId;
    /**
     *
     */
    @TableField(value = "product_sku_id")
    @ApiModelProperty(name = "productSkuId", value = "")
    private String productSkuId;
    /**
     *
     */
    @TableField(value = "member_id")
    @ApiModelProperty(name = "memberId", value = "")
    private String memberId;
    /**
     * 购买数量
     */
    @TableField(value = "quantity")
    @ApiModelProperty(name = "quantity", value = "购买数量")
    private Integer quantity;
    /**
     * 添加到购物车的价格
     */
    @TableField(value = "price")
    @ApiModelProperty(name = "price", value = "添加到购物车的价格")
    private BigDecimal price;
    /**
     * 销售属性1
     */
    @TableField(value = "sp1")
    @ApiModelProperty(name = "sp1", value = "销售属性1")
    private String sp1;
    /**
     * 销售属性2
     */
    @TableField(value = "sp2")
    @ApiModelProperty(name = "sp2", value = "销售属性2")
    private String sp2;
    /**
     * 销售属性3
     */
    @TableField(value = "sp3")
    @ApiModelProperty(name = "sp3", value = "销售属性3")
    private String sp3;
    /**
     * 商品主图
     */
    @TableField(value = "product_pic")
    @ApiModelProperty(name = "productPic", value = "商品主图")
    private String productPic;
    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    @ApiModelProperty(name = "productName", value = "商品名称")
    private String productName;
    /**
     * 商品副标题（卖点）
     */
    @TableField(value = "product_sub_title")
    @ApiModelProperty(name = "productSubTitle", value = "商品副标题（卖点）")
    private String productSubTitle;
    /**
     * 商品sku条码
     */
    @TableField(value = "product_sku_code")
    @ApiModelProperty(name = "productSkuCode", value = "商品sku条码")
    private String productSkuCode;
    /**
     * 会员昵称
     */
    @TableField(value = "member_nickname")
    @ApiModelProperty(name = "memberNickname", value = "会员昵称")
    private String memberNickname;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    @ApiModelProperty(name = "createDate", value = "创建时间")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField(value = "modify_date")
    @ApiModelProperty(name = "modifyDate", value = "修改时间")
    private Date modifyDate;
    /**
     * 是否删除
     */
    @TableField(value = "delete_status")
    @ApiModelProperty(name = "deleteStatus", value = "是否删除")
    private Integer deleteStatus;
    /**
     * 商品分类
     */
    @TableField(value = "product_category_id")
    @ApiModelProperty(name = "productCategoryId", value = "商品分类")
    private String productCategoryId;
    /**
     *
     */
    @TableField(value = "product_brand")
    @ApiModelProperty(name = "productBrand", value = "")
    private String productBrand;
    /**
     *
     */
    @TableField(value = "product_sn")
    @ApiModelProperty(name = "productSn", value = "")
    private String productSn;
    /**
     * 商品销售属性:[{"key":"颜色","value":"颜色"},{"key":"容量","value":"4G"}]
     */
    @TableField(value = "product_attr")
    @ApiModelProperty(name = "productAttr", value = "商品销售属性:颜色,容量")
    private String productAttr;

}
