package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限)
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_admin_permission_relation")
@ApiModel("后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
public class UmsAdminPermissionRelationEntity extends Model<UmsAdminPermissionRelationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "admin_id")
    @ApiModelProperty(name = "adminId", value = "")
    private String adminId;
    /**
     *
     */
    @TableField(value = "permission_id")
    @ApiModelProperty(name = "permissionId", value = "")
    private String permissionId;
    /**
     *
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "")
    private Integer type;

}
