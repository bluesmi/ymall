package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 产品评价回复表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_comment_replay")
@ApiModel("产品评价回复表")
public class PmsCommentReplayEntity extends Model<PmsCommentReplayEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "comment_id")
    @ApiModelProperty(name = "commentId", value = "")
    private String commentId;
    /**
     *
     */
    @TableField(value = "member_nick_name")
    @ApiModelProperty(name = "memberNickName", value = "")
    private String memberNickName;
    /**
     *
     */
    @TableField(value = "member_icon")
    @ApiModelProperty(name = "memberIcon", value = "")
    private String memberIcon;
    /**
     *
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "")
    private String content;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     * 评论人员类型；0->会员；1->管理员
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "评论人员类型；0->会员；1->管理员")
    private Integer type;

}
