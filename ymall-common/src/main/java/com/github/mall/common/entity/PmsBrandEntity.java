package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 品牌表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pms_brand")
@ApiModel("品牌表")
public class PmsBrandEntity extends Model<PmsBrandEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "name")
    @ApiModelProperty(name = "name", value = "")
    private String name;
    /**
     * 首字母
     */
    @TableField(value = "first_letter")
    @ApiModelProperty(name = "firstLetter", value = "首字母")
    private String firstLetter;
    /**
     *
     */
    @TableField(value = "sort")
    @ApiModelProperty(name = "sort", value = "")
    private Integer sort;
    /**
     * 是否为品牌制造商：0->不是；1->是
     */
    @TableField(value = "factory_status")
    @ApiModelProperty(name = "factoryStatus", value = "是否为品牌制造商：0->不是；1->是")
    private Integer factoryStatus;
    /**
     *
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "")
    private Integer showStatus;
    /**
     * 产品数量
     */
    @TableField(value = "product_count")
    @ApiModelProperty(name = "productCount", value = "产品数量")
    private Integer productCount;
    /**
     * 产品评论数量
     */
    @TableField(value = "product_comment_count")
    @ApiModelProperty(name = "productCommentCount", value = "产品评论数量")
    private Integer productCommentCount;
    /**
     * 品牌logo
     */
    @TableField(value = "logo")
    @ApiModelProperty(name = "logo", value = "品牌logo")
    private String logo;
    /**
     * 专区大图
     */
    @TableField(value = "big_pic")
    @ApiModelProperty(name = "bigPic", value = "专区大图")
    private String bigPic;
    /**
     * 品牌故事
     */
    @TableField(value = "brand_story")
    @ApiModelProperty(name = "brandStory", value = "品牌故事")
    private String brandStory;

}
