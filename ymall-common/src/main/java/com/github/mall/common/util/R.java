package com.github.mall.common.util;

import com.github.mall.common.constant.CommonConstants;
import com.github.mall.common.constant.enmus.CommontReturnEnums;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 统一返回的对象
 */
@Builder
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int code = CommonConstants.SUCCESS;

    @Getter
    @Setter
    private String msg = "success";


    @Getter
    @Setter
    private T data;

    public R() {
        super();
    }

    public R(T data) {
        super();
        this.data = data;
    }

    public R(T data, String msg) {
        super();
        this.data = data;
        this.msg = msg;
    }

    public R(Throwable e) {
        super();
        this.msg = e.getMessage();
        this.code = CommonConstants.FAIL;
    }

    public R(Throwable e, CommontReturnEnums commontReturnEnums) {
        super();
        this.msg = e.getMessage();
        this.code = commontReturnEnums.getCode();
    }

    public R(CommontReturnEnums commontReturnEnums) {
        this.code = commontReturnEnums.getCode();
        this.msg = commontReturnEnums.getMessage();
    }

    public R(CommontReturnEnums commontReturnEnums,T data) {
        this.code = commontReturnEnums.getCode();
        this.msg = commontReturnEnums.getMessage();
        this.data = data;
    }
}
