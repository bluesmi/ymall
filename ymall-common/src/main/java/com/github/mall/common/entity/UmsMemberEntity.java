package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 会员表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_member")
@ApiModel("会员表")
public class UmsMemberEntity extends Model<UmsMemberEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "member_level_id")
    @ApiModelProperty(name = "memberLevelId", value = "")
    private String memberLevelId;
    /**
     * 用户名
     */
    @TableField(value = "username")
    @ApiModelProperty(name = "username", value = "用户名")
    private String username;
    /**
     * 密码
     */
    @TableField(value = "password")
    @ApiModelProperty(name = "password", value = "密码")
    private String password;
    /**
     * 昵称
     */
    @TableField(value = "nickname")
    @ApiModelProperty(name = "nickname", value = "昵称")
    private String nickname;
    /**
     * 手机号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(name = "phone", value = "手机号码")
    private String phone;
    /**
     * 帐号启用状态:0->禁用；1->启用
     */
    @TableField(value = "status")
    @ApiModelProperty(name = "status", value = "帐号启用状态:0->禁用；1->启用")
    private Integer status;
    /**
     * 注册时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "注册时间")
    private Date createTime;
    /**
     * 头像
     */
    @TableField(value = "icon")
    @ApiModelProperty(name = "icon", value = "头像")
    private String icon;
    /**
     * 性别：0->未知；1->男；2->女
     */
    @TableField(value = "gender")
    @ApiModelProperty(name = "gender", value = "性别：0->未知；1->男；2->女")
    private Integer gender;
    /**
     * 生日
     */
    @TableField(value = "birthday")
    @ApiModelProperty(name = "birthday", value = "生日")
    private Date birthday;
    /**
     * 所做城市
     */
    @TableField(value = "city")
    @ApiModelProperty(name = "city", value = "所做城市")
    private String city;
    /**
     * 职业
     */
    @TableField(value = "job")
    @ApiModelProperty(name = "job", value = "职业")
    private String job;
    /**
     * 个性签名
     */
    @TableField(value = "personalized_signature")
    @ApiModelProperty(name = "personalizedSignature", value = "个性签名")
    private String personalizedSignature;
    /**
     * 用户来源
     */
    @TableField(value = "source_type")
    @ApiModelProperty(name = "sourceType", value = "用户来源")
    private Integer sourceType;
    /**
     * 积分
     */
    @TableField(value = "integration")
    @ApiModelProperty(name = "integration", value = "积分")
    private Integer integration;
    /**
     * 成长值
     */
    @TableField(value = "growth")
    @ApiModelProperty(name = "growth", value = "成长值")
    private Integer growth;
    /**
     * 剩余抽奖次数
     */
    @TableField(value = "luckey_count")
    @ApiModelProperty(name = "luckeyCount", value = "剩余抽奖次数")
    private Integer luckeyCount;
    /**
     * 历史积分数量
     */
    @TableField(value = "history_integration")
    @ApiModelProperty(name = "historyIntegration", value = "历史积分数量")
    private Integer historyIntegration;

}
