package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;

/**
 * 日志表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_log")
@ApiModel("日志表")
public class SysLogEntity extends Model<SysLogEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "编号")
    private String id;
    /**
     * 日志类型
     */
    @TableField(value = "type")
    @ApiModelProperty(name = "type", value = "日志类型")
    private String type;
    /**
     * 日志标题
     */
    @TableField(value = "title")
    @ApiModelProperty(name = "title", value = "日志标题")
    private String title;
    /**
     * 服务ID
     */
    @TableField(value = "service_id")
    @ApiModelProperty(name = "serviceId", value = "服务ID")
    private String serviceId;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    @ApiModelProperty(name = "createBy", value = "创建者")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Timestamp createTime;
    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private Timestamp updateTime;
    /**
     * 操作IP地址
     */
    @TableField(value = "remote_addr")
    @ApiModelProperty(name = "remoteAddr", value = "操作IP地址")
    private String remoteAddr;
    /**
     * 用户代理
     */
    @TableField(value = "user_agent")
    @ApiModelProperty(name = "userAgent", value = "用户代理")
    private String userAgent;
    /**
     * 请求URI
     */
    @TableField(value = "request_uri")
    @ApiModelProperty(name = "requestUri", value = "请求URI")
    private String requestUri;
    /**
     * 操作方式
     */
    @TableField(value = "method")
    @ApiModelProperty(name = "method", value = "操作方式")
    private String method;
    /**
     * 操作提交的数据
     */
    @TableField(value = "params")
    @ApiModelProperty(name = "params", value = "操作提交的数据")
    private String params;
    /**
     * 执行时间
     */
    @TableField(value = "time")
    @ApiModelProperty(name = "time", value = "执行时间")
    private String time;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(name = "delFlag", value = "删除标记")
    private String delFlag;
    /**
     * 异常信息
     */
    @TableField(value = "exception")
    @ApiModelProperty(name = "exception", value = "异常信息")
    private String exception;

}
