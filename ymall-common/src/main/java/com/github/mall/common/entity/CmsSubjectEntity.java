package com.github.mall.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 专题表
 *
 * @author bluesmi
 * @date 2019-06-25 19:49:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_subject")
@ApiModel("专题表")
public class CmsSubjectEntity extends Model<CmsSubjectEntity> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.UUID)
    @ApiModelProperty(name = "id", value = "")
    private String id;
    /**
     *
     */
    @TableField(value = "category_id")
    @ApiModelProperty(name = "categoryId", value = "")
    private String categoryId;
    /**
     *
     */
    @TableField(value = "title")
    @ApiModelProperty(name = "title", value = "")
    private String title;
    /**
     * 专题主图
     */
    @TableField(value = "pic")
    @ApiModelProperty(name = "pic", value = "专题主图")
    private String pic;
    /**
     * 关联产品数量
     */
    @TableField(value = "product_count")
    @ApiModelProperty(name = "productCount", value = "关联产品数量")
    private Integer productCount;
    /**
     *
     */
    @TableField(value = "recommend_status")
    @ApiModelProperty(name = "recommendStatus", value = "")
    private Integer recommendStatus;
    /**
     *
     */
    @TableField(value = "create_time")
    @ApiModelProperty(name = "createTime", value = "")
    private Date createTime;
    /**
     *
     */
    @TableField(value = "collect_count")
    @ApiModelProperty(name = "collectCount", value = "")
    private Integer collectCount;
    /**
     *
     */
    @TableField(value = "read_count")
    @ApiModelProperty(name = "readCount", value = "")
    private Integer readCount;
    /**
     *
     */
    @TableField(value = "comment_count")
    @ApiModelProperty(name = "commentCount", value = "")
    private Integer commentCount;
    /**
     * 画册图片用逗号分割
     */
    @TableField(value = "album_pics")
    @ApiModelProperty(name = "albumPics", value = "画册图片用逗号分割")
    private String albumPics;
    /**
     *
     */
    @TableField(value = "description")
    @ApiModelProperty(name = "description", value = "")
    private String description;
    /**
     * 显示状态：0->不显示；1->显示
     */
    @TableField(value = "show_status")
    @ApiModelProperty(name = "showStatus", value = "显示状态：0->不显示；1->显示")
    private Integer showStatus;
    /**
     *
     */
    @TableField(value = "content")
    @ApiModelProperty(name = "content", value = "")
    private String content;
    /**
     * 转发数
     */
    @TableField(value = "forward_count")
    @ApiModelProperty(name = "forwardCount", value = "转发数")
    private Integer forwardCount;
    /**
     * 专题分类名称
     */
    @TableField(value = "category_name")
    @ApiModelProperty(name = "categoryName", value = "专题分类名称")
    private String categoryName;

}
