package com.github.mall.admin.controller;

import cn.hutool.core.lang.Dict;
import com.github.mall.common.constant.enmus.CommontReturnEnums;
import com.github.mall.common.entity.UmsPermissionEntity;
import com.github.mall.common.services.UmsPermissionService;
import com.github.mall.common.util.R;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Api(tags="PermissionController",value = "权限控制器")
@RestController
@RequestMapping("/permission")
@AllArgsConstructor
@Slf4j
public class PermissionController {

    private final UmsPermissionService umsPermissionService;

    @ApiOperation(value = "增加权限接口")
    @RequestMapping(value = "/addPermission", method = RequestMethod.POST)
    @ResponseBody
    public R addPermission(@RequestBody Dict permissionParam){
        UmsPermissionEntity umsPermissionEntity = umsPermissionService.addPermission(permissionParam);
        if(umsPermissionEntity == null){
            return new R<String>(CommontReturnEnums.NOT_CREATED,"权限已经存在");
        }
        return new R<>(CommontReturnEnums.CREATED,umsPermissionEntity);
    }

}
