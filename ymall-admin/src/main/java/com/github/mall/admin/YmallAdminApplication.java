package com.github.mall.admin;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
@EnableSwagger2Doc
@ComponentScan("com.github.mall")
public class YmallAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(YmallAdminApplication.class, args);
    }

}
