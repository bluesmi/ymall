package com.github.mall.admin.controller;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.github.mall.common.config.JwtPropConfig;
import com.github.mall.common.constant.enmus.CommontReturnEnums;
import com.github.mall.common.constant.enmus.KafkaTopicEnums;
import com.github.mall.common.entity.UmsAdminEntity;
import com.github.mall.common.kafka.util.KafkaConsumerUtil;
import com.github.mall.common.services.UmsAdminService;
import com.github.mall.common.util.R;
import com.github.mall.common.util.RedisUtil;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Api(tags = "UmsAdminController", value = "后台用户管理")
@RestController
@RequestMapping("/admin")
@AllArgsConstructor
@Slf4j
public class UmsAdminController {

  private final UmsAdminService umsAdminService;
  private final JwtPropConfig jwtPropConfig;
  private final RedisUtil redisUtil;


  @ApiOperation(value = "用户注册")
  @ApiParam(name = "umsAdminParam", value = "注册入参", examples = @Example({
    @ExampleProperty(mediaType = "application/json", value = "{\"username\":\"用户名\",\"password\":\"密码\",\"icon\":\"用户头像\",\"email\":\"邮箱\",\"nickName\":\"用户昵称\",\"note\":\"备注\"}"),
  }))
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  @ResponseBody
  public R register(@RequestBody Dict umsAdminParam) {
    String captcha =umsAdminParam.getStr("verifyCode");
    String userName = umsAdminParam.getStr("username");
    String key = userName+"_"+captcha;
    if(!redisUtil.hasKey(key) || !StrUtil.equals(redisUtil.get(key).toString(),captcha)){
      return new R<String>(CommontReturnEnums.AUTH_CODE_WRONG, "注册验证码错误或已经失效！");
    }
    UmsAdminEntity umsAdmin = umsAdminService.register(umsAdminParam);
    if (umsAdmin == null) {
      return new R<String>(CommontReturnEnums.NOT_CREATED, "该用户名已经被注册！");
    }
    return new R<>(CommontReturnEnums.OK, umsAdmin);
  }

  @ApiOperation(value = "登录以后返回token")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "username", value = "用户名", dataType = "String"),
    @ApiImplicitParam(name = "password", value = "密码", dataType = "String")
  }
  )
  @RequestMapping(value = "/login", method = RequestMethod.POST)
  @ResponseBody
  public R login(@RequestBody Dict umsAdminLoginParam) {
    String token = umsAdminService.login(umsAdminLoginParam);
    if (token == null) {
      return new R<>(new Dict(), "用户名或密码错误");
    }
    Dict tokenMap = new Dict();
    tokenMap.put("token", token);
    tokenMap.put("tokenHead", jwtPropConfig.getTokenHead());
    return new R<>(CommontReturnEnums.OK, tokenMap);
  }


  @ApiOperation(value = "获取验证码")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "userMail", value = "用户邮箱", dataType = "String"),
  }
  )
  @RequestMapping(value = "/getAuthCode", method = RequestMethod.POST)
  @ResponseBody
  public R getAuthCode(@RequestBody Dict umsMailParam) {
    String captcha = RandomUtil.randomString(8);
    String userMail = umsMailParam.getStr("userMail");
//    Dict model = new Dict();
    umsMailParam.put("title", "ymall商城");
    umsMailParam.put("type", "注册");
    umsMailParam.put("captcha", captcha);
    umsMailParam.put("subject", "注册邮箱验证");
    KafkaConsumerUtil.sendMessage(KafkaTopicEnums.UMS_MAIL_TOPIC.getTopic(), umsMailParam);
    redisUtil.set(userMail+"_"+captcha,captcha,60*30);
    return new R<>(CommontReturnEnums.OK);
  }


}
