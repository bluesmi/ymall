package com.github.mall.admin.controller;

import com.github.mall.common.log.annotation.SysLog;
import com.github.mall.common.services.SysLogService;
import com.github.mall.common.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags="SysController",value = "日志管理")
@RestController
@RequestMapping("/syslog")
@AllArgsConstructor
@Slf4j
public class SysController {
    private final SysLogService sysLogService;

    @ApiOperation("测试系统日志")
    @SysLog("测试log")
    @GetMapping("/test")
    public void test() {

        log.error("进来了啊");
    }

    @ApiOperation("获取所有系统日志")
    @GetMapping("/getAllSysLog")
    public R getAllSysLog(){
        return new R<>(sysLogService.list());
    }
}
