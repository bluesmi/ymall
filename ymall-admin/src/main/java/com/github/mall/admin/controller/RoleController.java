package com.github.mall.admin.controller;

import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.github.mall.common.constant.enmus.CommontReturnEnums;
import com.github.mall.common.entity.UmsRoleEntity;
import com.github.mall.common.services.UmsPermissionService;
import com.github.mall.common.services.UmsRoleService;
import com.github.mall.common.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="RoleController",value = "角色控制器")
@RestController
@RequestMapping("/role")
@AllArgsConstructor
@Slf4j
public class RoleController {

    private final UmsRoleService umsRoleService;
    private final UmsPermissionService umsPermissionService;


    @ApiOperation(value = "增加角色接口")
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    @ResponseBody
    public R addRole(@RequestBody Dict roleParam){
        UmsRoleEntity umsRoleEntity = umsRoleService.addRole(roleParam);
        if(umsRoleEntity == null){
            return new R<String>(CommontReturnEnums.NOT_CREATED,"创建角色失败");
        }
        if(roleParam.containsKey("rolePermissionRelationList")){
            String str = roleParam.getStr("rolePermissionRelationList");
            List<String> list = JSONUtil.toList(JSONUtil.parseArray(str),String.class);
            umsPermissionService.addRolePermissionRalation(umsRoleEntity,list);
        }
        return new R<>(CommontReturnEnums.CREATED,umsRoleEntity);
    }
}
