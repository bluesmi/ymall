import fetch from "isomorphic-fetch";
import qs from "qs";
import store from "@/store";
import { Notification } from "element-ui";
const codeMessage = {
  200: "服务器成功返回请求的数据。",
  201: "新建或修改数据成功。",
  202: "一个请求已经进入后台排队（异步任务）。",
  204: "删除数据成功。",
  400: "发出的请求有错误，服务器没有进行新建或修改数据的操作。",
  401: "用户没有权限（令牌、用户名、密码错误）。",
  403: "用户得到授权，但是访问是被禁止的。",
  404: "发出的请求针对的是不存在的记录，服务器没有进行操作。",
  406: "请求的格式不可得。",
  410: "请求的资源被永久删除，且不会再得到的。",
  422: "当创建一个对象时，发生一个验证错误。",
  500: "服务器发生错误，请检查服务器。",
  502: "网关错误。",
  503: "服务不可用，服务器暂时过载或维护。",
  504: "网关超时。"
};

const checkStatus = async response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  let errortext = codeMessage[response.status] || response.statusText;
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  Notification({
    type: "error",
    title: "提示",
    message: error,
    duration: 3000
  });
  throw error;
};
/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [option] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, option) {
  const options = {
    ...option
  };
  const Authorization = store.state.user.token;
  const defaultOptions = {
    credentials: "include",
    headers: {
      Accept: "application/json; charset=utf-8",
      Authorization
    }
  };
  const newOptions = {
    ...defaultOptions,
    ...options
  };
  if (
    newOptions.method === "POST" ||
    newOptions.method === "PUT" ||
    newOptions.method === "DELETE"
  ) {
    if (!(newOptions.body instanceof FormData)) {
      newOptions.headers = {
        "Content-Type": "application/json; charset=utf-8",
        ...newOptions.headers
      };
      newOptions.body = JSON.stringify(newOptions.body);
    } else {
      // newOptions.body is FormData
      newOptions.headers = {
        ...newOptions.headers
      };
      newOptions.body = JSON.stringify(newOptions.body);
    }
  } else {
    if (newOptions.body !== undefined) {
      const body = JSON.parse(JSON.stringify(newOptions.body));
      newOptions.body = null;
      url = `${url}?${qs.stringify(body)}`;
    }
  }
  return fetch(url, newOptions)
    .then(checkStatus)
    .then(response => {
      if (newOptions.method === "DELETE" || response.status === 204) {
        if (
          // 登录和注册返回的是201 所以要排除登录和注册的情况 让他们走response.json()
          (response.status === 200 &&
            response.url.indexOf("api/admin/login") === -1) ||
          (response.status === 200 &&
            response.url.indexOf("api/admin/register") === -1)
        ) {
          return response.json();
        }
        if (newOptions.method === "DELETE" && response.status === 200) {
          return "";
        }
        return response.text();
      }
      return response.json();
    })
    .then(res => {
      if (res === "" || res === undefined || res === null) {
        return {
          success: true
        };
      }
      res.success = true;
      return res;
    })
    .catch(e => {
      const status = e.name;
      if (status === 401) {
        store.dispatch("user/resetToken").then(() => {
          location.reload(); // 为了重新实例化vue-router对象 避免bug
        });
        return;
      }
      return {
        success: false
      };
    });
}
