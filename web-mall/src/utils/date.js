export default function dateformat() {}

/**
 * 格式化时间
 * @param date
 * @param fmt
 * @returns {*}
 */
dateformat.format = function(date, fmt) {
  let o = {
    'M+': date.getMonth() + 1, //月份
    'd+': date.getDate(), //日
    'h+': date.getHours(), //小时
    'm+': date.getMinutes(), //分
    's+': date.getSeconds(), //秒
    'q+': Math.floor((date.getMonth() + 3) / 3), //季度
    S: date.getMilliseconds() //毫秒
  }
  if (!this.isNotEmpty(fmt)) {
    fmt = 'yyyy-MM-dd hh:mm:ss'
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + '').substr(4 - RegExp.$1.length)
    )
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
      )
    }
  }
  return fmt
}

dateformat.isNotEmpty = function(str) {
  if (str !== '' && str != null && typeof str != 'undefined') {
    return true
  }
  console.warn('argument format is wrong')
  return false
}

dateformat.addSeconds = seconds => {
  const time = new Date()
  const ms = time.getTime() // 转化为时间戳毫秒数
  
  time.setTime(ms + 1000 * seconds)
  return dateformat.format(time, 'yyyy-MM-dd hh:mm:ss')
}
