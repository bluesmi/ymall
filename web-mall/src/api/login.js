import request from '@/utils/request'
import {index, show, loginPost, add} from '@/api/common/index'

export function login(params) {
  return loginPost('/api/admin/login', params)
}
export function register(params) {
  return add('/api/admin/register', params)
}

export function getInfo() {
  return request({
    url: '/admin/info',
    method: 'get',
  })
}

export function logout() {
  return request({
    url: '/admin/logout',
    method: 'post'
  })
}
