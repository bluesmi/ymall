import request from '@/utils/request'


// 获取GET请求
export function index(url, params) {
    return request(`/${url}`, {
      method: 'GET',
      body: params,
    })
}
//获取带有id的GET请求
export function show(url, id, params) {
  return request(`/${url}/${id}`, {
    method: 'GET',
    body: params,
  })
}
//更新时候调用PUT请求
export function edit(url, id, params) {
  if (id === 0 || id === '' || id === '0') {
    return request(`/${url}`, {
      method: 'PUT',
      body: params,
    })
  }
  return request(`/${url}/${id}`, {
    method: 'PUT',
    body: params,
  });
}

// 新增的时候调用POST请求
export function add(url, params) {
  return request(`${url}`, {
    method: 'POST',
    body: params,
  })
}
// 登陆的时候调用POST请求
export function loginPost(url, params) {
  return request(`${url}`, {
    method: 'POST',
    body: params,
  })
}
//删除的时候调用DELETE请求
export function destroy(url, id, params) {
  if (id === 0 || id === '' || id === '0') {
    return request(`/${url}`, {
      method: 'DELETE',
      body: params,
    })
  }
  return request(`/${url}/${id}`, {
    method: 'DELETE',
    body: params,
  });
}
