import request from '@/utils/request'
import {index, show} from '@/api/common/index'

export function hello(params) {
  return request('/api/hello',{
    method: 'GET',
    body: params,
  })
}

export function test(params) {
  return request('/api/index/object',{
    method: 'GET',
    body: params,
  })
}


export function test1(params) {
  return index('api/index/object', params)
}

export function test2(id, params) {
  return show('api/index/getObject', id, params)
}
