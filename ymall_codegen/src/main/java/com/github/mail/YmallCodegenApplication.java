package com.github.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YmallCodegenApplication {

    public static void main(String[] args) {
        SpringApplication.run(YmallCodegenApplication.class, args);
    }

}
